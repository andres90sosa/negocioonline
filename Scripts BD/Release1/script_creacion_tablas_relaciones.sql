CREATE TABLE Roles(
IdRol INT NOT NULL CONSTRAINT PK_Roles_IdRol PRIMARY KEY,
NombreRol VARCHAR(50) NOT NULL
)

GO

CREATE TABLE Rubros(
IdRubro INT NOT NULL CONSTRAINT PK_Rubros_IdRubro PRIMARY KEY,
NombreRubro VARCHAR(100) NOT NULL,
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE Usuarios(
NombreUsuario VARCHAR(50) NOT NULL CONSTRAINT PK_Usuarios_NombreUsuario PRIMARY KEY,
Clave VARCHAR(50) NOT NULL,
Email VARCHAR(50) NOT NULL CONSTRAINT UK_Usuarios_Email UNIQUE,
FechaCambioClave DATETIME NOT NULL,
CantidadIntentos INT NOT NULL,
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE Provincias(
IdProvincia INT NOT NULL CONSTRAINT PK_Provincias_IdProvincia PRIMARY KEY,
NombreProvincia VARCHAR(50) NOT NULL
)

GO

CREATE TABLE Domicilios(
IdDomicilio INT NOT NULL CONSTRAINT PK_Domicilios_IdDomicilio PRIMARY KEY IDENTITY(1,1),
Calle VARCHAR(100) NOT NULL,
NroCalle INT NULL,
Piso INT NULL,
Dpto VARCHAR(50) NULL,
Barrio VARCHAR(100) NOT NULL,
Localidad VARCHAR(100) NOT NULL,
IdProvincia INT NOT NULL CONSTRAINT FK_Domicilios_Provincias_IdProvincia FOREIGN KEY REFERENCES Provincias(IdProvincia),
CodigoPostal INT NULL,
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE Negocios(
CodNegocio INT NOT NULL CONSTRAINT PK_Negocios_CodNegocio PRIMARY KEY IDENTITY(1,1),
RazonSocial VARCHAR(100) NOT NULL,
NroCuit BIGINT NULL,
IdDomiclio INT NOT NULL CONSTRAINT FK_Negocios_Domicilios_IdDomicilio FOREIGN KEY REFERENCES Domicilios(IdDomicilio),
IdRubro INT NOT NULL CONSTRAINT FK_Negocios_Rubros_IdRubro FOREIGN KEY REFERENCES Rubros(IdRubro),
NroTelFijo VARCHAR(50) NULL,
NroCel VARCHAR(50) NULL,
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE MarcasProducto(
IdMarcaProd INT NOT NULL CONSTRAINT PK_MarcasProducto_IdMarcaProd PRIMARY KEY IDENTITY(1,1),
NombreMarcaProd VARCHAR(100) NOT NULL,
CodNegocio INT NOT NULL CONSTRAINT FK_MarcasProducto_Negocios_CodNegocio FOREIGN KEY REFERENCES Negocios(CodNegocio),
IsDeleted BIT NOT NULL,
)

GO

CREATE TABLE TiposProducto(
IdTipoProd INT NOT NULL CONSTRAINT PK_TiposProducto_IdTipoProd PRIMARY KEY IDENTITY(1,1),
NombreTipoProd VARCHAR(100) NOT NULL,
IdTipoProdPadre INT NULL CONSTRAINT FK_TiposProducto_TiposProducto_IdTipoProdPadre FOREIGN KEY REFERENCES TiposProducto(IdTipoProd),
Nivel INT NOT NULL,
CodNegocio INT NOT NULL CONSTRAINT FK_TiposProducto_Negocios_CodNegocio FOREIGN KEY REFERENCES Negocios(CodNegocio),
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE Productos(
IdProducto INT NOT NULL CONSTRAINT PK_Productos_IdProducto PRIMARY KEY IDENTITY(1,1),
NombreProducto VARCHAR(100) NOT NULL,
IdTipoProd INT NOT NULL CONSTRAINT FK_Productos_TiposProducto_IdTipoProd FOREIGN KEY REFERENCES TiposProducto(IdTipoProd),
IdMarcaProd INT NULL CONSTRAINT FK_Productos_MarcasProducto_IdMarcaProd FOREIGN KEY REFERENCES MarcasProducto(IdMarcaProd),
PrecioCosto DECIMAL(10,2) NULL,
PrecioVenta DECIMAL(10,2) NULL,
PorcentajeGanancia DECIMAL(5,2) NULL,
CodigoBarra VARCHAR(100) NULL,
CodNegocio INT NOT NULL CONSTRAINT FK_Productos_Negocios_CodNegocio FOREIGN KEY REFERENCES Negocios(CodNegocio),
Imagen VARCHAR(MAX) NULL,
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE Comerciantes(
CodComerciante INT NOT NULL CONSTRAINT PK_Comerciantes_CodComerciante PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Apellido VARCHAR(50) NOT NULL,
NroCuit INT NULL,
NombreUsuario VARCHAR(50) NOT NULL CONSTRAINT FK_Comerciantes_Usuarios_NombreUsuario FOREIGN KEY REFERENCES Usuarios(NombreUsuario),
NroTelFijo VARCHAR(50) NULL,
NroCel VARCHAR(50) NULL,
IsDeleted BIT NOT NULL
)

GO

CREATE TABLE ComerciantesxNegocios(
CodComerciante INT NOT NULL CONSTRAINT FK_ComerciantesxNegocios_Comerciantes_CodComerciante FOREIGN KEY REFERENCES Comerciantes(CodComerciante),
CodNegocio INT NOT NULL CONSTRAINT FK_ComerciantesxNegocios_Negocios_CodNegocio FOREIGN KEY REFERENCES Negocios(CodNegocio),
IdRol INT NOT NULL CONSTRAINT FK_ComerciantesxNegocios_Roles_IdRol FOREIGN KEY REFERENCES Roles(IdRol),
IsDeleted BIT NOT NULL,
CONSTRAINT PK_ComerciantesxNegocios_CodComerciante_CodNegocio PRIMARY KEY(CodComerciante,CodNegocio)
)