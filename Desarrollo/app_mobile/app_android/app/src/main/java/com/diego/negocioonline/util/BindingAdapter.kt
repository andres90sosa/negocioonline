package com.diego.negocioonline.util

import android.graphics.BitmapFactory
import android.util.Base64
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.diego.negocioonline.R
import com.diego.negocioonline.network.Product
import com.diego.negocioonline.ui.product.ApiStatus
import com.diego.negocioonline.ui.product.ProductAdapter

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri =
            imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image))
            .into(imgView)
    }
}

@BindingAdapter("imageBase64")
fun bindBase64Image(imgView: ImageView, imgBase64: String) {
    if(imgBase64.isEmpty()) {
        imgView.setImageResource(R.drawable.ic_broken_image)
    }
    else {
        val imageBytes = Base64.decode(imgBase64, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        imgView.setImageBitmap(decodedImage)
    }
}

@BindingAdapter("listProducts")
fun bindRecyclerView(recyclerView: RecyclerView,
                     data: List<Product>?) {
    val adapter = recyclerView.adapter as ProductAdapter
    adapter.submitList(data)

}

@BindingAdapter("stateSearchProduct")
fun bindStatus(stateTextView: TextView,
               status: Boolean) {
    if(status) {
        stateTextView.visibility = View.INVISIBLE
    }
    else {
        stateTextView.visibility = View.VISIBLE
    }
}

@BindingAdapter("apiStatus")
fun bindApiStatus(statusImageView: ImageView,
               status: ApiStatus?) {
    when (status) {
        ApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        ApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        ApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}