package com.diego.negocioonline.ui.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.diego.negocioonline.network.Product
import com.diego.negocioonline.network.ProductsApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

enum class ApiStatus { LOADING, ERROR, DONE }

/**
 * Contiene toda la logica de negocio del fragmento ProductFragment.
 *
 * @author Sosa Ludueña Diego
 * @version 1.0
 */
class ProductViewModel : ViewModel() {

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(
        viewModelJob + Dispatchers.Main )

    private val _products = MutableLiveData<List<Product>>()

    val products: LiveData<List<Product>>
        get() = _products

    private val _selectedProduct = MutableLiveData<Product>()

    val selectedProduct: LiveData<Product>
        get() = _selectedProduct

    private val _stateProduct = MutableLiveData<Boolean>()

    val stateProduct: LiveData<Boolean>
        get() = _stateProduct

    private val _statusApi = MutableLiveData<ApiStatus>()

    val statusApi: LiveData<ApiStatus>
        get() = _statusApi

    /**
     * Inicializa el viewModel.
     */
    init {
        _stateProduct.value = true

        getProducts("Fanta")
    }

    /**
     * Este metodo solicita una consulta al ApiService para guardar en _products la lista de productos.
     * Ademas, se registran los estados que va teniendo la consulta hacia el ApiService para
     * ir mostrando por la interfaz de usuario el avance del mismo haciendo uso de la
     * variable _statusApi.
     */
    private fun getProducts(filter: String) {
        coroutineScope.launch {
            var getProductsDeferred = ProductsApi.retrofitService.getProducts(1, filter, "")
            try {
                _statusApi.value = ApiStatus.LOADING
                var listResult = getProductsDeferred.await()
                _statusApi.value = ApiStatus.DONE
                _products.value = listResult
            } catch (e: Exception) {
                _statusApi.value = ApiStatus.ERROR
                _products.value = ArrayList()
            }
        }
    }

    /**
     * En caso que el usuario presione sobre un producto de la lista de
     * RecyclerView, se guardara el preducto seleccionado en _selectedProduct.
     * De esta manera se prodra navegar hacia el fragmeto ProductDetailFragment.
     */
    fun navigateSelectedProduct(product: Product) {
        _selectedProduct.value = product
    }

    /**
     * Una vez que se navego hacia el fragmento ProductDetailFragment,
     * se coloca en nulo _selectedProduct.
     */
    fun navigateSelectedProductCompleted() {
        _selectedProduct.value = null
    }

    /**
     * Este metodo realiza la busqueda al ApiService dependiendo del query que introdujo
     * el usuario en la intefaz de usuario.
     */
    fun filter(query: String) {
        getProducts(query)
    }

    /**
     * Se utiliza para destruir el viewModel cuando se destruye el fragmento.
     * De esta manera se libera espacio. Y tambien se cancela alguna corutina que
     * se este realizando.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}