package com.diego.negocioonline.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Se crea constante BASE_URL para contener la URL del web service
 */
//private const val BASE_URL = "http://192.168.43.62:49809/api/"
private const val BASE_URL = "https://be7d8eaa-fffd-4ca5-88c1-0f45370eb6fa.mock.pstmn.io/api/"

/**
 * Se construye un objeto moshi.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

/**
 * Se construye un objeto retrofit.
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

/**
 * La interfaz ApiService contiene los metodos para consultar las diferentes peticiones
 * al web service.
 *
 * @author Sosa Ludueña Diego
 * @version 1.0
 */
interface ApiService {
    /**
     * Se consulta a API para devolver los productos dependiendo de la palabra clave
     * que solicito el usuario. La busqueda se realiza por el nombre de negocio o codigo de barra.
     * @return retorna lista de productos.
     */
    @GET("productos/obtenerproductos")
    fun getProducts(@Query("codNegocio") codNegocio: Int,
                    @Query("nombre") nombre: String,
                    @Query("codBarra") codBarra: String):
            Deferred<List<Product>>
}

/**
 * Instacia que crea comunicacion con API.
 */
object ProductsApi {
    val retrofitService : ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}

