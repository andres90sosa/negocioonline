package com.diego.negocioonline.ui.product_detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.diego.negocioonline.network.Product

class ProductDetailViewModel (product: Product,
                              app: Application
) : AndroidViewModel(app) {

    private val _selectedProduct = MutableLiveData<Product>()
    val selectedProduct: LiveData<Product>
        get() = _selectedProduct

    init {
        _selectedProduct.value = product
    }
}