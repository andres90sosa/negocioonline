package com.diego.negocioonline.ui.stock


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.diego.negocioonline.databinding.FragmentStockBinding

/**
 * A simple [Fragment] subclass.
 */
class StockFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentStockBinding.inflate(inflater)
        return binding.root
    }


}
