package com.diego.negocioonline.ui.product_detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.diego.negocioonline.R

import com.diego.negocioonline.databinding.FragmentProductDetailBinding
import com.diego.negocioonline.network.Product
import com.diego.negocioonline.ui.edit_product.EditProductFragment

/**
 * A simple [Fragment] subclass.
 */
class ProductDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        val binding = FragmentProductDetailBinding.inflate(inflater)
        binding.setLifecycleOwner(this)

        val product = ProductDetailFragmentArgs.fromBundle(arguments!!).selectedProduct
        val viewModelFactory = DetalleProductoViewModelFactory(product, application)
        val viewModel = ViewModelProviders.of(
            this, viewModelFactory).get(ProductDetailViewModel::class.java)
        binding.viewModel = viewModel

        binding.fabProductDetail.setOnClickListener { view ->
            val dialog = EditProductFragment()
            val bundle = Bundle()
            bundle.putParcelable(dialog.PRODUCT, product)
            bundle.putInt(dialog.FAB, R.id.fab_product_detail)
            dialog.arguments = bundle
            val ft = fragmentManager?.beginTransaction()
            ft?.let { dialog.show(it,  dialog.TAG) }
        }

        return binding.root
    }


}
