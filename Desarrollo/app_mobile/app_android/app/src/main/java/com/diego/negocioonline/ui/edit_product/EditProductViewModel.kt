package com.diego.negocioonline.ui.edit_product

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.diego.negocioonline.network.Product

class EditProductViewModel (product: Product,
                            app: Application
) : AndroidViewModel(app) {
    private val _editProduct = MutableLiveData<Product>()
    val editProduct: LiveData<Product>
        get() = _editProduct

    init {
        _editProduct.value = product
    }
}