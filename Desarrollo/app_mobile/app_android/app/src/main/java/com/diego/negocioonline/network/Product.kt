package com.diego.negocioonline.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

/**
 * La clase de dato Product tiene referencia a los atributos del JSON Productos.
 * Es decir, se realiza la serializacion para tener un mejor tratamiento.
 *
 * @author Sosa Ludueña Diego
 * @version 1.0
 */
@Parcelize
data class Product(
    @Json(name = "IdProd") val id: Int = -1,
    @Json(name = "Nombre") val product_nombre: String = "",
    @Json(name = "CodBarra") val product_codigo_barra: String = "",
    @Json(name = "Imagen") val product_imagen: String = "",
    @Json(name = "TipoProd") val product_tipo: String = "",
    @Json(name = "MarcaProd") val product_marca: String = "",
    @Json(name = "Precioventa") val product_precio_venta: Double = 0.0,
    @Json(name = "PrecioCosto") val product_precio_costo: Double = 0.0,
    @Json(name = "PorcentajeGanancia") val product_porcentaje_ganancia: Int = 0) : Parcelable