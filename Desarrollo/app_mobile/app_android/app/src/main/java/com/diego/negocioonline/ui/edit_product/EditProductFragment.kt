package com.diego.negocioonline.ui.edit_product


import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import com.diego.negocioonline.R
import com.diego.negocioonline.databinding.FragmentEditProductBinding
import com.diego.negocioonline.network.Product
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

/**
 * Esta clase EditProductFragment tiene la funcion de mostrar
 * los campos para editar producto,
 * guardar producto existente o no existente,
 * Corroborar que los datos de los campos sean validos,
 * mostrar dialogos y SnackBar indicando el resultado del proceso.
 *
 * @author Sosa Ludueña Diego
 * @version 1.0
 */
class EditProductFragment : DialogFragment() {

    val TAG = "EDIT_PRODUCT_FRAGMENT"

    val PRODUCT = "PRODUCT_PROPERTY"

    val FAB = "FAB_SNACKBAR"

    private lateinit var binding: FragmentEditProductBinding

    private lateinit var product: Product

    private lateinit var fab: FloatingActionButton

    /**
     * Se configura antes de ser visible fragmento de dialogo
     * para que se muestre pantalla completa.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    /**
     * Crea y devuelve la jerarquía de vistas asociada con el fragmento.
     * @return retorna jerarquía de vistas asociada con el fragmento.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        binding = FragmentEditProductBinding.inflate(inflater)
        binding.setLifecycleOwner(this)

        product = arguments?.get(PRODUCT) as Product
        fab = activity?.findViewById(arguments?.getInt(FAB)!!)!!

        val viewModelFactory = EditProductViewModelFactory(product, application)
        val viewModel = ViewModelProviders.of(
            this, viewModelFactory).get(EditProductViewModel::class.java)
        binding.viewModel = viewModel

        /**
         * Se añade un titulo al toolbar.
         */
        binding.toolbarEditProduct.setTitle(R.string.nav_edit_product)

        /**
         * Se infla el menu de opciones al toolbar.
         */
        binding.toolbarEditProduct.inflateMenu(R.menu.edit_product_menu)

        /**
         * Cada vez que se enfoca en el campo nombre de producto,
         * se quita la advertencia de error.
         */
        binding.editProductNombreProducto.editText?.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus) {
                binding.editProductNombreProducto.error = null
            }
        }

        /**
         * Cada vez que se enfoca en el campo codigo de barra,
         * se quita la advertencia de error.
         */
        binding.editProductCodigoBarra.editText?.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus) {
                binding.editProductCodigoBarra.error = null
            }
        }

        /**
         * Cada vez que se enfoca en el campo precio de costo,
         * se quita la advertencia de error.
         */
        binding.editProductPrecioCosto.editText?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.editProductPrecioCosto.error = null
            }
        }

        /**
         * Cada vez que se enfoca en el campo precio de venta,
         * se quita la advertencia de error.
         */
        binding.editProductPrecioVenta.editText?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.editProductPrecioVenta.error = null
            }
        }

        onSave()
        onCancel()
        onBackPressed()

        return binding.root
    }

    private fun onSave() {
        binding.toolbarEditProduct.setOnMenuItemClickListener{
            when(it.itemId) {
                R.id.edit_product_guardar -> {
                    validate()
                }
            }
            onContextItemSelected(it)
        }
    }

    private fun onCancel() {
        binding.toolbarEditProduct.setNavigationOnClickListener { view ->
            checkForCancel()
        }
    }

    private fun validate() {
        val product_name = binding.editProductNombreProducto.editText?.text.toString()
        val product_type = binding.editProductTipoProducto.editText?.text.toString()
        val product_mark = binding.editProductMarcaProducto.editText?.text.toString()
        val product_codebar = binding.editProductCodigoBarra.editText?.text.toString()
        val product_sale_cost = binding.editProductPrecioCosto.editText?.text.toString()
        val product_sale_price = binding.editProductPrecioVenta.editText?.text.toString()

        if(compareProducts()) {
            Snackbar.make(fab, getString(R.string.message_snackbar_nothing_to_keep), Snackbar.LENGTH_LONG).show()
            dismiss()
        }
        else {
            if(product_name.isEmpty() || product_name.length < 3 ||
                product_codebar.isEmpty() || product_type.isEmpty() ||
                product_mark.isEmpty() || product_sale_cost.isEmpty() || product_sale_cost.startsWith(".") || product_sale_price.isEmpty() || product_sale_price.startsWith(".")) {
                Snackbar.make(binding.root, getString(R.string.message_snackbar_check_fields), Snackbar.LENGTH_LONG).show()

                if(product_name.isEmpty() || product_name.length < 3) {
                    binding.editProductNombreProducto.error = getString(R.string.error_text_input_product_name)
                }
                if(product_codebar.isEmpty()) {
                    binding.editProductCodigoBarra.error = getString(R.string.error_text_input_product_barcode)
                }
                if(product_type.isEmpty()) {
                    binding.editProductTipoProducto.error = getString(R.string.error_text_input_product_type)
                }
                if(product_mark.isEmpty()) {
                    binding.editProductMarcaProducto.error = getString(R.string.error_text_input_product_mark)
                }
                if(product_sale_cost.isEmpty() || product_sale_cost.startsWith(".")) {
                    binding.editProductPrecioCosto.error = getString(R.string.error_text_input_cost_price)
                }
                if(product_sale_price.isEmpty() || product_sale_price.startsWith(".")) {
                    binding.editProductPrecioVenta.error = getString(R.string.error_text_input_sale_price)
                }
            }
            else {
                Snackbar.make(fab, getString(R.string.message_snackbar_saved_product), Snackbar.LENGTH_LONG)
                    //.setAnchorView(fab)
                    .show()
                dismiss()
            }
        }
    }

    private fun checkForCancel() {
        if(compareProducts()) {
            dismiss()
        }
        else {
            MaterialAlertDialogBuilder(context)
                .setTitle(getString(R.string.title_dialog_discard_changes))
                .setMessage(getString(R.string.message_dialog_discard_changes))
                .setNegativeButton(getString(R.string.button_dialog_cancel)) { dialog, which ->
                    // Respond to negative button press
                    dialog.dismiss()
                }
                .setPositiveButton(getString(R.string.button_dialog_discard)) { dialog, which ->
                    // Respond to positive button press
                    Snackbar.make(fab, getString(R.string.message_snackbar_discard_changes), Snackbar.LENGTH_LONG)
                        //.setAnchorView(fab)
                        .show()
                    dismiss()

                }
                .show()
        }
    }

    private fun compareProducts(): Boolean =
        product.product_nombre.equals(binding.editProductNombreProducto.editText?.text.toString()) &&
                product.product_codigo_barra.equals(binding.editProductCodigoBarra.editText?.text.toString()) &&
                product.product_precio_costo.toString().equals(binding.editProductPrecioCosto.editText?.text.toString()) &&
                product.product_precio_venta.toString().equals(binding.editProductPrecioVenta.editText?.text.toString())

    private fun onBackPressed() {
        dialog?.setOnKeyListener { dialog, keyCode, event ->
            if(keyCode == KeyEvent.KEYCODE_BACK) {
                if(event.action!= KeyEvent.ACTION_DOWN) {
                    return@setOnKeyListener true
                }
                else {
                    checkForCancel()
                    return@setOnKeyListener true
                }
            }
            else {
                return@setOnKeyListener false
            }
        }
    }
}
