package com.diego.negocioonline.ui.product


import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.diego.negocioonline.R
import com.diego.negocioonline.databinding.FragmentProductBinding
import com.diego.negocioonline.network.Product
import com.diego.negocioonline.ui.edit_product.EditProductFragment
import com.google.android.material.snackbar.Snackbar

/**
 * Esta clase ProductFragment tiene la funcion de mostrar
 * lista de producto de forma explicita,
 * mostar las opciones por el menu de la barra de accion,
 * realizar busquedas de producto por nombre de producto o por codigo de barra,
 * Añadir un nuevo producto y
 * Consumir web service para consultar los productos.
 *
 * @author Sosa Ludueña Diego
 * @version 1.0
 */
class ProductFragment : Fragment() {

    private lateinit var searchItem: MenuItem
    private lateinit var searchView: SearchView

    /**
     * Se utiliza viewModel para los cambios de configuracion del dispositivo.
     * De esta manera se mantienen los datos en la interfaz de usuario y se encapsulan los datos
     * de la logica de negocio.
     */
    private val viewModel: ProductViewModel by lazy {
        ViewModelProviders.of(this).get(ProductViewModel::class.java)
    }

    /**
     * Crea y devuelve la jerarquía de vistas asociada con el fragmento.
     * @return retorna jerarquía de vistas asociada con el fragmento.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentProductBinding.inflate(inflater)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel

        /**
         * Se muestra la lista de productos del RecyclerView y si se pulsa sobre algun producto de la lista
         * actualiza el producto seleccionado selectedProduct.
         */
        binding.recyclerViewProduct.adapter = ProductAdapter(ProductAdapter.OnClickListener {
            viewModel.navigateSelectedProduct(it)
        })

        /**
         * El fragmento ProductFragment observa el producto seleccionado selectedProduct, cada vez
         * que se pulsa sobre algun producto de la lista del RecyclerView.
         * Luego se navega al fragmento ProductDetailFragment pasandole como argumento
         * el producto seleccionado.
         */
        viewModel.selectedProduct.observe(this, Observer {
            if ( null != it ) {
                this.findNavController().navigate(ProductFragmentDirections.actionProductFragmentToProductDetailFragment(it))
                viewModel.navigateSelectedProductCompleted()
            }
        })

        /**
         * El fragmento ProductFragment observa la variable statusApi para determinar si no fue
         * posible conectarse con el servicio web.
         * En caso que se produzca un error, mostrara un SnackBar para que vuelva a intentarlo.
         */
        viewModel.statusApi.observe(this, Observer {
            if ( it == ApiStatus.ERROR ) {
                Snackbar.make(binding.fabProduct, "Comprobar la conexión de red", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Reintentar", View.OnClickListener {
                        viewModel.filter("Recientes")
                    })
                    .show()
            }
        })

        /**
         * Cuando el usuario pulsa en el FAB se crea un el fragmento de dialogo
         * EditProductFragment para editar el producto.
         * Se pasa por parametro un producto vacio para que se pueda crear exitosamente
         * EditProductFragment.
         * Tambien se pasa por parametro el FAB para saber a que vista mostrar un SnackBar.
         */
        binding.fabProduct.setOnClickListener { view: View ->
            val newProduct = Product()
            val dialog = EditProductFragment()
            val bundle = Bundle()
            bundle.putParcelable(dialog.PRODUCT, newProduct)
            bundle.putInt(dialog.FAB, R.id.fab_product)
            dialog.arguments = bundle
            val ft = fragmentManager?.beginTransaction()
            ft?.let { dialog.show(it,  dialog.TAG) }
        }

        /**
         * Se habilita opciones de menu a fragmento ProductFragment
         */
        setHasOptionsMenu(true)

        return binding.root
    }

    /**
     * Este metodo se sobrescribe para crear el menú de opciones
     * cuando el usuario abre el menú por primera vez.
     * Muestra los elementos de la barra de app.
     * La finalidad es configurar la interfaz de busqueda y la logica de la misma.
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.product_menu, menu)

        /**
         * Se crea y configura SearchView.
         */
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchItem = menu.findItem(R.id.search)
        searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
        searchView.maxWidth = Int.MAX_VALUE

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            // Para buscar cuando aprieta en buscar o enter desde teclado
            override fun onQueryTextSubmit(query: String?): Boolean {
                /**
                 * La busqueda query se pasa a minuscula
                 * y se quitan los espacios en los extremos.
                 * Luego se llama a la funcion filter para efectuar la busqueda.
                 * Por ultimo, cuando se pulsa en buscar se quita el foco de SearchView
                 * y se quita el teclado de la pantalla.
                 */
                query?.toLowerCase()?.trim()?.let { viewModel.filter(it) }
                searchView.clearFocus()
                searchItem.collapseActionView()
                return true
            }

            // Para buscar cada vez que añade un nuevo caracter
            override fun onQueryTextChange(newText: String?): Boolean {
                //newText?.toLowerCase()?.trim()?.let { viewModel.filter(it) }
                return false
            }

        })
    }
}
