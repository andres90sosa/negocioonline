package com.diego.negocioonline.ui.report


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.diego.negocioonline.databinding.FragmentReportBinding

/**
 * A simple [Fragment] subclass.
 */
class ReportFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentReportBinding.inflate(inflater)
        return binding.root
    }


}
