﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;

namespace MLContract
{
    /// <summary>
    /// Define firmas de métodos para las operaciones CRUD (Create, Read, Update y Delete) de la entidad Producto.
    /// </summary>
    public interface IServicioProducto : IServicio<Producto> 
    {
        IEnumerable<Producto> ObtenerPorNombreAproximado(int codNegocio, string nombre);
        Producto ObtenerPorNombre(string nombre, bool incluirEliminados);
        Producto ObtenerPorCodigoDeBarra(int codNegocio, string codigoDeBarra);
    }
}
