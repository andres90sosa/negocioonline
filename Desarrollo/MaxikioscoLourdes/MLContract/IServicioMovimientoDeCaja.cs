﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;

namespace MLContract
{
    /// <summary>
    /// Define firmas de métodos para las operaciones CRUD (Create, Read, Update y Delete) de la entidad MovimientoDeCaja.
    /// </summary>
    public interface IServicioMovimientoDeCaja :IServicio<MovimientoDeCaja>
    {
        List<MovimientoDeCaja> ObtenerVentas(DateTime fecha);
    }
}
