﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;

namespace MLContract
{
    /// <summary>
    /// Define firmas de métodos para las operaciones CRUD (Create, Read, Update y Delete) de la entidad Tipo de Producto.
    /// </summary>
    public interface IServicioTipoProducto: IServicio<TipoProducto>
    {
        IEnumerable<TipoProducto> ObtenerPorNivel(int nivel,int? idTipoProductoPadre);
        IEnumerable<TipoProducto> ObtenerJerarquiaDeTipos(int idTipoProducto);
    }
}
