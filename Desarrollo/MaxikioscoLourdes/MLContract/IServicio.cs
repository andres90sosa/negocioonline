﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MLContract
{
    /// <summary>
    /// Define firmas de métodos para las operaciones CRUD (Create, Read, Update y Delete) de una clase genérica T.
    /// </summary>
    /// <typeparam name="T">Clase genérica para la cual se definen las firmas de métodos.</typeparam>
    public interface IServicio<T> where T:class
    {
        void Guardar(T entity);
        void Eliminar(int id);
        IEnumerable<T> ObtenerTodo(bool incluirEliminados);
        T Buscar(Expression<Func<T, bool>> predicate);
        IEnumerable<T> Filtrar(Expression<Func<T, bool>> predicate);
    }
}
