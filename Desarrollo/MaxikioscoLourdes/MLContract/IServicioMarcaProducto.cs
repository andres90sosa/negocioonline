﻿using MLDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLContract
{
    public interface IServicioMarcaProducto : IServicio<MarcaProducto>
    {
        IEnumerable<MarcaProducto> ObtenerPorCantidad(int codNegocio, int cantidad);

        bool Actualizar(MarcaProducto marcaProducto);

        bool Borrar(int Id);
    }
}
