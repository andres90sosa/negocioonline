﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MLDomain;
using MLContract;
using MLService;


namespace MLCustomControls
{
    public partial class UCBuscarProducto : UserControl
    {
        private IEnumerable<Producto> listaProductos;
        private IServicioProducto servicioProducto;
        private int contadorTeclaUp;
        private int contadorTeclaDown;

        public UCBuscarProducto()
        {
            InitializeComponent();
        }

        #region Propiedades

        public Producto ProductoSeleccionado { get; private set; }

        #endregion

        #region Eventos

        public delegate void MyEventClick();

        /// <summary>
        /// Evento personalizado que se dipara cuando se busca un producto.
        /// </summary>
        public event MyEventClick EventClickBtnBuscar;

        /// <summary>
        /// Maneja el evento cuando se carga el control de usuario <see cref="UCBuscarProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void UCBuscarProducto_Load(object sender, EventArgs e)
        {
            txtProducto.Focus();
            pListaProductos.Visible = false;
            servicioProducto = new ServicioProducto();
        }

        /// <summary>
        /// Maneja el evento cuando cambia el texto del campo <see cref="txtProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtProducto_TextChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAutoComplete();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al buscar producto.", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se suelta una tecla en el campo de texto <see cref="txtProducto"/>.
        /// Cuando se suelta la tecla Down o Up y la lista de productos tiene items se hace foco sobre la lista, 
        /// se selecciona el primer o último item de la lista según sea el caso y se incrementa un contador.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtProducto_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (lbProductos.Items.Count > 0 && (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up))
                {
                    lbProductos.Focus();

                    if (e.KeyCode == Keys.Down)
                    {
                        lbProductos.SelectedIndex = 0;
                        contadorTeclaUp++;
                    }
                    else
                    {
                        lbProductos.SelectedIndex = lbProductos.Items.Count - 1;
                        contadorTeclaDown++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al presionar una tecla en el campo producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se presiona una tecla en el campo de texto <see cref="txtProducto"/>.
        /// Cuando se presiona la tecla Enter se llama al método <see cref="BuscarProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    BuscarProducto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al buscar un producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se presiona una tecla en la lista de productos.
        /// Maneja el desplazamiento por los items de la lista incrementando  y reiniciando contadores.
        /// Cuando se presiona la tecla Enter se llama a la función <see cref="SeleccionarProductoDeLista"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void lbProductos_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    SeleccionarProductoDeLista();
                else
                {
                    if (e.KeyCode == Keys.Up && lbProductos.SelectedIndex == 0)
                    {
                        contadorTeclaUp++;
                        contadorTeclaDown = 0;
                    }
                    else if (e.KeyCode == Keys.Down && lbProductos.SelectedIndex == lbProductos.Items.Count - 1)
                    {
                        contadorTeclaDown++;
                        contadorTeclaUp = 0;
                    }
                    else
                    {
                        contadorTeclaUp = 0;
                        contadorTeclaDown = 0;
                    }

                    if (contadorTeclaUp == 2 || contadorTeclaDown == 2 || ((e.KeyCode == Keys.Up || e.KeyCode == Keys.Down) && lbProductos.Items.Count == 1))
                    {
                        lbProductos.SelectedIndex = -1;
                        txtProducto.Focus();
                        txtProducto.SelectionStart = txtProducto.Text.Length;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al presionar tecla en la lista de productos", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se pierde el foco de la lista de productos. Llama al método <see cref="SeleccionarProductoDeLista"/> 
        /// y reinicia los contadores <see cref="contadorTeclaDown"/> y <see cref="contadorTeclaUp"/> .
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void lbProductos_Leave(object sender, EventArgs e)
        {
            try
            {
                SeleccionarProductoDeLista();
                contadorTeclaDown = 0;
                contadorTeclaUp = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al perder foco de la lista de productos", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace doble click sobre la lista de productos. 
        /// Llama al método <see cref="SeleccionarProductoDeLista"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void lbProductos_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SeleccionarProductoDeLista();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al hacer doble click sobre la lista de productos", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se pierde el foco del campo <see cref="txtProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtProducto_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!lbProductos.Focused)
                    pListaProductos.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al perder foco del campo producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnBuscar"/>.
        /// Llama al método <see cref="BuscarProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                BuscarProducto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al buscar un producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        #endregion

        #region MetodosDelFormulario
        /// <summary>
        /// Carga y muestra una lista de productos a medida que se va ingresando 
        /// el nombre del producto en el campo de texto <see cref="txtProducto"/>.
        /// </summary>
        protected void LoadAutoComplete()
        {
            try
            {
                lbProductos.Items.Clear();
                listaProductos = null;

                if (txtProducto.Text.Trim().Length < 3)
                    pListaProductos.Visible = false;
                else
                {
                    listaProductos = servicioProducto.ObtenerPorNombreAproximado(txtProducto.Text);

                    foreach (Producto p in listaProductos)
                        lbProductos.Items.Add(p.Nombre);

                    if (lbProductos.Items.Count > 0)
                        pListaProductos.Visible = true;
                    else
                        pListaProductos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error al cargar función de autocompletado. " + ex.Message);
            }
        }

        /// <summary>
        /// Setea el texto del campo producto con el item seleccionado de la lista de productos,
        /// oculta la lista y hace foco sobre el campo de texto producto.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        protected void SeleccionarProductoDeLista()
        {
            try
            {
                if (pListaProductos.Visible && lbProductos.SelectedIndex != -1)
                {
                    txtProducto.Text = lbProductos.SelectedItem.ToString();
                    pListaProductos.Visible = false;
                    txtProducto.Focus();
                    txtProducto.SelectionStart = txtProducto.Text.Length;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Busca un producto por su nombre o por su código de barra. Dispara el evento <see cref="EventClickBtnBuscar"/> si existe el producto.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        protected void BuscarProducto()
        {
            bool buscarPorCodigo = false;
            int codigoBarra = 0;
            string nombreProducto = "";
            ProductoSeleccionado = null;
            try
            {
                if (!string.IsNullOrEmpty(txtProducto.Text.Trim()))
                {
                    nombreProducto = txtProducto.Text.Trim();
                    buscarPorCodigo = int.TryParse(nombreProducto, out codigoBarra);

                    if (buscarPorCodigo)
                        ProductoSeleccionado = servicioProducto.ObtenerPorCodigoDeBarra(nombreProducto);
                    else
                        ProductoSeleccionado = servicioProducto.ObtenerPorNombre(nombreProducto, false);

                    if (ProductoSeleccionado == null)
                    {
                        MessageBox.Show("El producto no existe. Verifique por favor.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                        txtProducto.Focus();
                    }
                    else
                        EventClickBtnBuscar?.Invoke();
                }
                else
                {
                    MessageBox.Show("Ingrese el nombre o el código de barra del producto.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    txtProducto.Focus();
                }                    
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Hace foco sobre el campo de texto <see cref="txtProducto"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        public void FocusTextBoxProducto()
        {
            try
            {
                txtProducto.Focus();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Limpia el campo de texto <see cref="txtProducto"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        public void ClearTextBoxProducto()
        {
            try
            {
                txtProducto.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Habilita o deshabilita el campo de texto <see cref="txtProducto"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        public void EnabledTextBox(bool enabled)
        {
            try
            {
                if (enabled)
                    txtProducto.Enabled = true;
                else
                    txtProducto.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Habilita o deshabilita el botón buscar.
        /// </summary>
        /// <param name="enabled">Si se setea en <c>true</c> habilita el botón y si se setea en <c>false</c> deshabilita el botón.</param>
        /// <exception cref="ApplicationException"></exception>
        public void EnabledBotonBuscar(bool enabled)
        {
            try
            {
                HabilitarBoton(btnBuscar, enabled);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Habilita o deshabilita un botón.
        /// </summary>
        /// <param name="boton">Boton que se habilita o deshabilita.</param>
        /// <param name="habilitar">Si se setea en <c>true</c> se habilita el boton. Si se setea en <c>false</c> se deshabilita el boton.</param>
        private void HabilitarBoton(Button boton, bool habilitar)
        {
            try
            {
                if (habilitar)
                {
                    boton.Enabled = true;
                    boton.BackColor = Color.FromArgb(40, 167, 69);
                }
                else
                {
                    boton.Enabled = false;
                    boton.BackColor = Color.LightGray;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        #endregion
    }
}
