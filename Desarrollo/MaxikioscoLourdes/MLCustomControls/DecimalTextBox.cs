﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace MLCustomControls
{
    /// <summary>
    /// Esta clase personaliza un control de campo de texto para que solo permita ingresar números decimales.
    /// </summary>
    [ToolboxBitmap(typeof(TextBox))]
    public partial class DecimalTextBox: TextBox
    {
        public DecimalTextBox()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Maneja el evento cuando se presiona una tecla en el campo de texto.
        /// Valida que la tecla presionada tenga el formato de un número decimal.
        /// </summary>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            char symbol = ',';
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != symbol)
                    e.Handled = true;
                else if (e.KeyChar == symbol && this.Text.IndexOf(symbol) > -1)
                    e.Handled = true;
                else if (e.KeyChar == symbol && this.Text.Length == 0)
                    e.Handled = true;
                else if(this.Text.Split(',').Count() == 1)
                {
                    if (this.Text.Length > 3 && e.KeyChar != symbol && !char.IsControl(e.KeyChar))
                        e.Handled = true;
                }
                else if(this.Text.Split(',').Count() == 2 && e.KeyChar != symbol && !char.IsControl(e.KeyChar))
                {
                    int posCharSymbol = this.Text.IndexOf(',');
                    int posCharCursor = this.SelectionStart;

                    if(posCharCursor > posCharSymbol)
                    {
                        if (!string.IsNullOrEmpty(this.Text.Split(',')[1]) && this.Text.Split(',')[1].Length > 1)
                            e.Handled = true;
                    }
                    else if(!string.IsNullOrEmpty(this.Text.Split(',')[0]) && this.Text.Split(',')[0].Length > 3)
                    {
                        e.Handled = true;
                    }
                }

                base.OnKeyPress(e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al presionar tecla en el campo de texto " + this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se pierde el foco del campo de texto. 
        /// Formatea el texto del campo para que se visualice con el formato definido de número decimal. 
        /// Si el valor ingresado no cumple con el formato de número decimal o es mayor o igual a 10.000 se limpia el campo.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/>Instancia que contiene los datos del evento.</param>
        protected override void OnLeave(EventArgs e)
        {
            decimal valor = 0;
            try
            {
                if (decimal.TryParse(this.Text, out valor))
                    if (valor < 10000)
                        this.Text = valor.ToString("N2").Replace(".", "");
                    else
                        this.Text = "";
                else
                    this.Text = "";

                base.OnLeave(e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al perder el foco del campo de texto " + this.Name, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
