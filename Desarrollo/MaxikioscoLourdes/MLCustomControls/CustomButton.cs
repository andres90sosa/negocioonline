﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MLCustomControls
{
    public partial class CustomButton : Button
    {
        public CustomButton()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Maneja el evento cuando se situa el mouse sobre el botón.
        /// Cambia el color del texto a naranja.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        protected override void OnMouseHover(EventArgs e)
        {
            this.ForeColor = Color.FromArgb(229, 126, 49);
            base.OnMouseHover(e);
        }

        /// <summary>
        /// Maneja el evento cuando se retira el mouse del botón.
        /// Cambia el color del texto a gris.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            this.ForeColor = Color.LightGray;
            base.OnMouseLeave(e);
        }
    }
}
