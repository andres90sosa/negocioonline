﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{
    /// <summary>
    /// Esta clase mapea en un objeto los datos de un detalle de movimiento de caja.
    /// </summary>
    public class DetalleMovimientoDeCaja : Base
    {

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DetalleMovimientoDeCaja"/>.
        /// </summary>
        public DetalleMovimientoDeCaja()
        {
            Id = 0;
            IsDeleted = false;
            IdMovimiento = 0;
            IdProducto = 0;
            Cantidad = 0;
            PrecioVentaUnitario = 0;
            MovimientoDeCaja = null;
            Producto = null;
        }

        /// <summary>
        /// Obtiene o setea el identificador de movimiento de caja.
        /// </summary>
        /// <value>
        /// El identificador de movimiento de caja.
        /// </value>
        public int IdMovimiento { get; set; }

        /// <summary>
        /// Obtiene o setea el identificador de producto.
        /// </summary>
        /// <value>
        /// El identificador de producto.
        /// </value>
        public int IdProducto { get; set; }

        /// <summary>
        /// Obtiene o setea la cantidad.
        /// </summary>
        /// <value>
        /// La cantidad.
        /// </value>
        public int Cantidad { get; set; }

        /// <summary>
        /// Obtiene o setea el precio de venta unitario.
        /// </summary>
        /// <value>
        /// El precio de venta unitario.
        /// </value>
        public decimal PrecioVentaUnitario { get; set; }

        /// <summary>
        /// Obtiene o setea el objeto movimiento de caja.
        /// </summary>
        /// <value>
        /// El objeto movimiento de caja.
        /// </value>
        public virtual MovimientoDeCaja MovimientoDeCaja { get; set; }

        /// <summary>
        /// Obtiene o setea el objeto producto.
        /// </summary>
        /// <value>
        /// El producto.
        /// </value>
        public virtual Producto Producto { get; set; }

        /// <summary>
        /// Obtiene el subtotal.
        /// Se calcula realizando la multiplicación del precio de venta por la cantidad.
        /// </summary>
        public decimal Subtotal
        {
            get { return (PrecioVentaUnitario * Cantidad); }
        }
        
    }
}
