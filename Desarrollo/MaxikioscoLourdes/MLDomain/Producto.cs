﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{
    /// <summary>
    /// Esta clase mapea en un objeto los datos de un producto.
    /// </summary>
    /// <seealso cref="MLDomain.Base" />
    public class Producto:Base
    {

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Producto"/>. Constructor con parámetros.
        /// </summary>
        /// /// <param name="id">Identificador de producto.</param>
        /// <param name="nombre">Nombre del producto.</param>
        /// <param name="idTipo">Identificador de tipo de producto.</param>
        /// <param name="idMarca">Identificador de marca de producto.</param>
        /// <param name="precioCosto">Precio de costo.</param>
        /// <param name="precioVenta">Precio de venta.</param>
        /// <param name="porcentajeGanancia">Porcentaje de ganancia.</param>
        /// <param name="codigoBarra">Código de barra.</param>
        public Producto(int id,string nombre, int idTipo,int? idMarca,decimal? precioCosto,decimal? precioVenta,decimal? porcentajeGanancia, string codigoBarra)
        {
           
            this.Id = id;
            this.Nombre = nombre;
            this.IdTipoProd = idTipo;
            this.IdMarcaProd = idMarca;
            this.PrecioCosto = precioCosto;
            this.PrecioVenta = precioVenta;
            this.PorcentajeGanancia = porcentajeGanancia;
            this.CodigoBarra = codigoBarra;
            this.IsDeleted = false;
        }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="Producto"/>. Constructor sin parámetros.
        /// </summary>
        public Producto() { }

        /// <summary>
        /// Obtiene o setea el nombre del producto.
        /// </summary>
        /// <value>
        /// Nombre del producto.
        /// </value>
        public string Nombre { get; set; }

        /// <summary>
        /// Obtiene o setea el identificador de tipo de producto.
        /// </summary>
        /// <value>
        /// Id tipo producto.
        /// </value>
        public int IdTipoProd { get; set; }

        /// <summary>
        /// Obtiene o setea el identificador de marca de producto.
        /// </summary>
        /// <value>
        /// Id marca producto.
        /// </value>
        public int? IdMarcaProd { get; set; }

        /// <summary>
        /// Obtiene o setea el precio de costo del producto.
        /// </summary>
        /// <value>
        /// Precio de costo del producto.
        /// </value>
        public decimal? PrecioCosto { get; set; }

        /// <summary>
        /// Obtiene o setea el precio de venta.
        /// </summary>
        /// <value>
        /// Precio de venta del producto.
        /// </value>
        public decimal? PrecioVenta { get; set; }

        /// <summary>
        /// Obtiene o setea el porcentaje de ganancia del producto.
        /// </summary>
        /// <value>
        /// Porcentaje de ganancia del producto.
        /// </value>
        public decimal? PorcentajeGanancia { get; set; }

        /// <summary>
        /// Obtiene o setea el codigo de barra.
        /// </summary>
        /// <value>
        /// El codigo barra.
        /// </value>
        public string CodigoBarra { get; set; }

        /// <summary>
        /// Obtiene o setea el código de negocio del producto.
        /// </summary>
        /// <value>
        /// Código de negocio del producto.
        /// </value>
        public int CodNegocio { get; set; }


        /// <summary>
        /// Obtiene o setea la imagen del producto.
        /// </summary>
        /// <value>
        /// Imagen del producto.
        /// </value>
        public string Imagen { get; set; }

        /// <summary>
        /// Obtiene o setea un objeto tipo de producto.
        /// </summary>
        /// <value>
        /// Objeto tipo de producto.
        /// </value>
        public virtual TipoProducto Tipo { get; set; }
        
        /// <summary>
        /// Obtiene o setea un objeto marca de producto.
        /// </summary>
        /// <value>
        /// Objeto marca de producto.
        /// </value>
        public virtual MarcaProducto Marca { get; set; }

    }
}
