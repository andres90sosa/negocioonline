﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{

    /// <summary>
    /// Esta clase mapea en un objeto los datos de un movimiento de caja.
    /// </summary>
    /// <seealso cref="MLDomain.Base" />
    public class MovimientoDeCaja : Base
    {
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="MovimientoDeCaja"/>.  Constructor con parámetros.
        /// </summary>
        /// <param name="fecha">Fecha.</param>
        /// <param name="isDeleted">Indica si el movimiento esta o no eliminado.</param>
        /// <param name="idTipoMovimiento">Identificador de tipo de movimiento.</param>
        /// <param name="detallesMovimientoDeCaja">Lista de detalles de movimiento.</param>
        public MovimientoDeCaja(DateTime fecha, bool isDeleted,int idTipoMovimiento, List<DetalleMovimientoDeCaja> detallesMovimientoDeCaja)
        {
            Fecha = fecha;
            IsDeleted = isDeleted;
            IdTipoMovimiento = idTipoMovimiento;
            DetallesMovimientoDeCaja = detallesMovimientoDeCaja;
        }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="MovimientoDeCaja"/>. Constructor sin parámetros.
        /// </summary>
        public MovimientoDeCaja()
        {
            Id = 0;
            IsDeleted = false;
            Fecha = DateTime.MinValue;
            Total = 0;
            IdTipoMovimiento = 0;
            DetallesMovimientoDeCaja = new List<DetalleMovimientoDeCaja>();
            TipoMovimiento = null;
        }

        /// <summary>
        /// Obtiene o setea la fecha.
        /// </summary>
        /// <value>
        /// La fecha.
        /// </value>
        public DateTime Fecha { get; set; }

        /// <summary>
        /// Obtiene o setea el total.
        /// </summary>
        public decimal Total{ get; set; }


        /// <summary>
        /// Obtiene o setea el identificador de tipo de movimiento de caja.
        /// </summary>
        /// <value>
        /// El identificador de tipo de movimiento de caja.
        /// </value>
        public int IdTipoMovimiento { get; set; }

        /// <summary>
        /// Obtiene o setea la lista de detalles de movimiento de caja.
        /// </summary>
        /// <value>
        /// La lista de detalles de movimiento de caja.
        /// </value>
        public virtual List<DetalleMovimientoDeCaja> DetallesMovimientoDeCaja { get; set; }

        /// <summary>
        /// Obtiene o setea el objeto tipo de movimiento de caja.
        /// </summary>
        /// <value>
        /// El objeto tipo de movimiento de caja.
        /// </value>
        public virtual TipoMovimientoDeCaja TipoMovimiento { get; set; }


        /// <summary>
        /// Obtiene una lista personalizada con los datos de los detalles de movimiento de caja.
        /// </summary>
        /// <returns>
        /// Devuelve una lista personalizada con los datos de los detalles de movimiento de caja.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public object ObtenerDatosDetallesMovimientoDeCaja()
        {
            try
            {
                var result = from detalle in DetallesMovimientoDeCaja
                             select new
                             {
                                 Producto = detalle.Producto.Nombre,
                                 detalle.Cantidad,
                                 detalle.PrecioVentaUnitario,
                                 detalle.Subtotal,
                                 PrecioVentaAux = detalle.Producto.PrecioVenta,
                                 IdProducto = detalle.Producto.Id
                             };

                return result.ToList();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Calcula el total del movimiento realizando la suma de los subtotales de todos sus detalles y lo setea en la propiedad <see cref="Total"/>.
        /// </summary>
        public void CalcularTotal()
        {
            try
            {
                Total = 0;

                foreach (var detalle in DetallesMovimientoDeCaja)
                {
                    Total += detalle.Subtotal;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

    }
}
