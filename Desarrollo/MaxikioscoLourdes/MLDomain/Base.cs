﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{
    /// <summary>
    /// Esta clase mapea en un objeto que herede de la misma el identificador 
    /// y la propiedad IsDeleted que indica si la instancia fue eliminada o no.
    /// </summary>
    public abstract class Base
    {
        /// <summary>
        /// Obtiene o setea el identificador.
        /// </summary>
        /// <value>
        /// El identificador.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Obtiene o setea un valor indicando si la instancia esta eliminada.
        /// </summary>
        /// <value>
        ///   <c>true</c> si la instancia esta eliminada; en caso contrario, <c>false</c>.
        /// </value> 
        public bool IsDeleted { get; set; }
    }
}
