﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{
    /// <summary>
    /// Esta clase mapea en un objeto los datos de un tipo de producto.
    /// </summary>
    /// <seealso cref="MLDomain.Base" />
    public class TipoProducto:Base
    {

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TipoProducto"/>.
        /// </summary>
        public TipoProducto()
        {
            Nombre = "";
            IdTipoProdPadre = 0;
            Nivel = 0;
            TipoDeProductoPadre = null;
        }

        /// <summary>
        /// Obtiene o setea el nombre del tipo de producto.
        /// </summary>
        /// <value>
        /// El nombre.
        /// </value>
        public string Nombre { get; set; }

        /// <summary>
        /// Obtiene o setea el identificador de tipo de producto padre.
        /// </summary>
        /// <value>
        /// El identificador de tipo de producto padre.
        /// </value>
        public int? IdTipoProdPadre { get; set; }

        /// <summary>
        /// Obtiene o setea el nivel del tipo de producto.
        /// </summary>
        /// <value>
        /// Nivel del tipo de producto.
        /// </value>
        public int Nivel { get; set; }

        /// <summary>
        /// Obtiene o setea el código de negocio del tipo de producto.
        /// </summary>
        /// <value>
        /// Código de negocio del tipo de producto.
        /// </value>
        public int CodNegocio { get; set; }

        /// <summary>
        /// Obtiene or setea el objeto tipo de producto padre.
        /// </summary>
        /// <value>
        /// Objeto tipo de producto padre.
        /// </value>
        public virtual TipoProducto TipoDeProductoPadre { get; set; }
        
    }
}
