﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{
    /// <summary>
    /// Esta clase mapea en un objeto los datos de la marca de un producto.
    /// </summary>
    /// <seealso cref="MLDomain.Base" />
    public class MarcaProducto:Base
    {
        public MarcaProducto() { }
        public MarcaProducto(int? id, string nombre)
        {
            Id = (int)(id == null ? 0 : id);
            Nombre = nombre;
        }
        /// <summary>
        /// Obtiene o setea el nombre.
        /// </summary>
        /// <value>
        /// Nombre.
        /// </value>
        public string Nombre { get; set; }

        /// <summary>
        /// Obtiene o setea el código de negocio.
        /// </summary>
        /// <value>
        /// Código de negocio.
        /// </value>
        public int CodNegocio { get; set; }

        /// <summary>
        /// Obtiene o setea la fecha de creación.
        /// </summary>
        /// <value>
        /// Fecha de creación.
        /// </value>
        public DateTime FechaCreacion { get; set; }

        /// <summary>
        /// Obtiene o setea la fecha de modificación.
        /// </summary>
        /// <value>
        /// Fecha de modificación.
        /// </value>
        public DateTime? FechaModificacion { get; set; }
    }
}
