﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLDomain
{
    /// <summary>
    /// Esta clase mapea en un objeto los datos de un tipo de movimiento de caja.
    /// </summary>
    /// <seealso cref="MLDomain.Base" />
    public class TipoMovimientoDeCaja:Base
    {
        /// <summary>
        /// Obtiene o setea el nombre.
        /// </summary>
        /// <value>
        /// El nombre.
        /// </value>
        public string Nombre { get; set; }
    }
}
