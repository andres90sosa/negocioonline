﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;
using MLDataAnnotation;


namespace MLDataAccess
{
    public class MLContext:DbContext
    {
        public MLContext() : base("DbConnectionString") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Configurations.Add(new ConfiguracionProducto());
            modelBuilder.Configurations.Add(new ConfiguracionMarcaProducto());
            modelBuilder.Configurations.Add(new ConfiguracionTipoProducto());
            modelBuilder.Configurations.Add(new ConfiguracionDetalleMovimientoDeCaja());
            modelBuilder.Configurations.Add(new ConfiguracionMovimientoDeCaja());
            modelBuilder.Configurations.Add(new ConfiguracionTipoMovimientoDeCaja());
            base.OnModelCreating(modelBuilder);
        }
      
        public DbSet<TipoProducto> TiposProducto { get; set; }
        public DbSet<MarcaProducto> MarcasProducto { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<DetalleMovimientoDeCaja> DetallesMovimientosDeCaja { get; set; }
        public DbSet<MovimientoDeCaja> MovimientosDeCaja { get; set; }
        public DbSet<TipoMovimientoDeCaja> TiposMovimientoDeCaja { get; set; }

    }
}
