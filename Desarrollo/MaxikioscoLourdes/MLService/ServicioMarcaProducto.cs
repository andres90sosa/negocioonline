﻿using MLContract;
using MLDomain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLService
{
    public class ServicioMarcaProducto : Servicio<MarcaProducto>, IServicioMarcaProducto
    {
        public IEnumerable<MarcaProducto> ObtenerPorCantidad(int codNegocio, int cantidad)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.MarcasProducto.Where(x => x.CodNegocio == codNegocio && x.IsDeleted == false)
                                          .OrderBy(x => x.Nombre).Take(cantidad)
                                          .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        public bool Actualizar(MarcaProducto marcaActualizada)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    MarcaProducto marca = context.MarcasProducto.Find(marcaActualizada.Id);

                    if (marca == null)
                        return false;
                    else
                    { 
                        if (marca.Nombre != marcaActualizada.Nombre.Trim())
                        {
                            marca.Nombre = marcaActualizada.Nombre.Trim();
                            marca.FechaModificacion = DateTime.Now;
                            context.Entry(marca).State = EntityState.Modified;
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return true;
        }

        public bool Borrar(int Id) 
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    MarcaProducto marca = context.MarcasProducto.Find(Id);
                    if (marca != null)
                    {
                        marca.IsDeleted = true;
                        context.Entry(marca).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    else
                        return false;
                    
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return true;
        }
    }
}
