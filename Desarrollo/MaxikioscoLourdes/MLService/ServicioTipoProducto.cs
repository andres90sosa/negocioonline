﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;
using MLContract;
using System.Data.SqlClient;

namespace MLService
{
    /// <summary>
    /// Implementa los métodos de la interfaz <see cref="IServicioTipoProducto"/> para las operaciones CRUD (Create, Read, Update y Delete) de la entidad Tipo de Producto.
    /// Hereda de la clase <see cref="Servicio{TipoDeProducto}"/>.
    /// </summary>
    public class ServicioTipoProducto : Servicio<TipoProducto>, IServicioTipoProducto
    {
        /// <summary>
        ///  Obtiene una lista con la jerarquía de tipos de producto para el id de tipo especificado.
        /// </summary>
        /// <param name="idTipoProducto">Id de tipo de producto para el que se buscará la jerarquia de tipos.</param>
        /// <returns>
        /// Devuelve una lista de tipos de producto.
        /// </returns>
        public IEnumerable<TipoProducto> ObtenerJerarquiaDeTipos(int idTipoProducto)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.Database.SqlQuery<TipoProducto>("EXEC sp_ObtenerJerarquiaTiposDeProducto @IdTipoProducto", new SqlParameter("IdTipoProducto", idTipoProducto)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene una lista de tipos de producto que coincidan con el nivel y el id de tipo de producto padre especificados.
        /// </summary>
        /// <param name="nivel">El nivel de tipo de producto a buscar.</param>
        /// <param name="idTipoProductoPadre">El identificador de tipo de producto padre a buscar.</param>
        /// <returns>Devuelve una lista de tipos de producto.</returns>
        /// <exception cref="ApplicationException"></exception>
        public IEnumerable<TipoProducto> ObtenerPorNivel(int nivel, int? idTipoProductoPadre)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.TiposProducto.Include("TipoDeProductoPadre")
                                  .Where(t => t.Nivel == nivel && t.IdTipoProdPadre == idTipoProductoPadre)
                                  .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
