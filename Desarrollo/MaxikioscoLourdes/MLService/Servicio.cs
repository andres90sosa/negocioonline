﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MLContract;

namespace MLService
{
    /// <summary>
    /// Implementa los métodos de la interfaz <see cref="IServicio{T}"/> para las operaciones CRUD (Create, Read, Update  y Delete)
    /// de una clase genérica T (clase Base).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Servicio<T> : Base, IServicio<T> where T : MLDomain.Base
    {
        /// <summary>
        /// Guarda la entidad especificada.
        /// </summary>
        /// <param name="entity">La entidad a guardar.</param>
        /// <exception cref="ApplicationException"></exception>
        public virtual void Guardar(T entity)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    if (entity.Id == 0)
                    {
                        context.Set<T>().Add(entity);
                    }
                    else
                    {
                        context.Set<T>().Attach(entity);
                        context.Entry(entity).State = EntityState.Modified;
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Busca y obtiene la primer entidad que satisfaga la expresión de búsqueda.
        /// </summary>
        /// <param name="predicate">La expresión de busqueda.</param>
        /// <returns>
        /// Devuelve la primer entidad que satisfaga la expresión de búsqueda.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public virtual T Buscar(Expression<Func<T, bool>> predicate)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.Set<T>().FirstOrDefault(predicate);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Elimina la entidad que tenga el identificador especificado.
        /// </summary>
        /// <param name="id">El identificador de la entidad a eliminar.</param>
        /// <exception cref="ApplicationException"></exception>
        public virtual void Eliminar(int id)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    var entity = context.Set<T>().Find(id);
                    entity.IsDeleted = true;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Busca y obtiene una lista de entidades que satisfagan la expresión de busqueda.
        /// </summary>
        /// <param name="predicate">La expresión de busqueda.</param>
        /// <returns>
        /// Devuelve una lista de entidades que satisfagan la expresión de busqueda.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public virtual IEnumerable<T> Filtrar(Expression<Func<T, bool>> predicate)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.Set<T>().Where(predicate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene todas las entidades. 
        /// </summary>
        /// <param name="incluirEliminados">
        /// Si se setea en <c>true</c> se incluyen en el resultado las entidades eliminadas. 
        /// Si se setea en <code>false</code> solo se incluyen en el resultado las entidades disponibles (NO eliminadas).
        /// </param>
        /// <returns>Devuelve todas las entidades.</returns>
        /// <exception cref="ApplicationException"></exception>
        public virtual IEnumerable<T> ObtenerTodo(bool incluirEliminados)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    if (incluirEliminados)
                        return context.Set<T>().ToList();
                    
                    else
                        return context.Set<T>().Where(e => e.IsDeleted == false).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
