﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLContract;
using MLDomain;
using MLDataAccess;
using System.Data.Entity;
using System.Linq.Expressions;

namespace MLService
{
    /// <summary>
    /// Implementa los métodos de la interfaz <see cref="IServicioProducto"/> para las operaciones CRUD (Create, Read, Update y Delete) de la entidad Producto.
    /// Hereda de la clase <see cref="Servicio{Producto}"/>.
    /// </summary>
    public class ServicioProducto : Servicio<Producto>, IServicioProducto
    {
        /// <summary>
        /// Obtiene una lista de productos para el negocio indicado donde cada producto contenga en su nombre la cadena especificada.
        /// </summary>
        /// <param name="codNegocio">Código de negocio.</param>
        /// <param name="nombre">Nombre del producto.</param>
        /// <returns>
        /// Devuelve una lista de productos para el negocio indicado.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public IEnumerable<Producto> ObtenerPorNombreAproximado(int codNegocio, string nombre)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.Productos.Include(p => p.Tipo)
                                            .Include(p => p.Tipo.TipoDeProductoPadre)
                                            .Include(p => p.Marca)
                                            .Where(p => p.Nombre.ToUpper().Contains(nombre.Trim().ToUpper()) && p.IsDeleted == false &&
                                                        p.CodNegocio == codNegocio)
                                            .ToList();
                                            
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene un producto cuyo nombre coincida con la cadena especificada.
        /// </summary>
        /// <param name="nombre">Nombre del producto a buscar.</param>
        /// <param name="incluirEliminados">
        /// Si se setea en <code>true</code> se incluye en el resultado los productos eliminados.
        /// Si se setea en <code>false</code> solo se incluyen en el resultado los productos disponibles (NO eliminados).
        /// </param>
        /// <returns>Retorna un producto cuyo nombre coincida con la cadena especificada.</returns>
        /// <exception cref="ApplicationException"></exception>
        public Producto ObtenerPorNombre(string nombre, bool incluirEliminados)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    if (incluirEliminados)
                    {
                        return context.Productos.Include(p => p.Tipo)
                                            .Include(p => p.Tipo.TipoDeProductoPadre)
                                            .Include(p => p.Marca)
                                            .FirstOrDefault(p => p.Nombre.ToUpper() == nombre.Trim().ToUpper());
                    }
                    else
                    {
                        return context.Productos.Include(p => p.Tipo)
                                            .Include(p => p.Tipo.TipoDeProductoPadre)
                                            .Include(p => p.Marca)
                                            .FirstOrDefault(p => p.Nombre.ToUpper() == nombre.Trim().ToUpper() && p.IsDeleted == false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene un producto para el negocio indicado por código de barra.
        /// </summary>
        /// <param name="codNegocio">Código de negocio.</param>
        /// <param name="codigoDeBarra">Código de barra del producto.</param>
        /// <returns>
        /// Devuelve un producto para el negocio indicado.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public Producto ObtenerPorCodigoDeBarra(int codNegocio, string codigoDeBarra)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.Productos.Include(p => p.Tipo)
                                            .Include(p => p.Tipo.TipoDeProductoPadre)
                                            .Include(p => p.Marca)
                                            .FirstOrDefault(p => p.CodigoBarra == codigoDeBarra.Trim() && p.IsDeleted == false &&
                                                                 p.CodNegocio == codNegocio);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
