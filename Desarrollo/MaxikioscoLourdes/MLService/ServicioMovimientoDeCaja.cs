﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;
using System.Data.Entity;
using MLContract;


namespace MLService
{
    /// <summary>
    /// Implementa los métodos de la interfaz <see cref="IServicioMovimientoDeCaja"/> para las operaciones CRUD (Create, Read, Update y Delete) de la entidad Movimiento de Caja.
    /// Hereda de la clase <see cref="Servicio{MovimientoDeCaja}"/>.
    /// </summary>
    public class ServicioMovimientoDeCaja :Servicio<MovimientoDeCaja>, IServicioMovimientoDeCaja
    {
        /// <summary>
        /// Guarda el movimiento de caja especificado. Sobreescribe el método heredado de la clase <see cref="Servicio{MovimientoDeCaja}"/>.
        /// </summary>
        /// <param name="movimiento">El movimiento de Caja a guardar.</param>
        /// <exception cref="ApplicationException"></exception>
        public override void Guardar(MovimientoDeCaja movimiento)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            if (movimiento.Id == 0)
                            {
                                foreach (var detalle in movimiento.DetallesMovimientoDeCaja)
                                {
                                   // Se setea el objeto producto de cada detalle en null para que no sea tenido en cuenta en el guardado de un movimiento de caja.
                                    detalle.Producto = null;
                                }

                                context.MovimientosDeCaja.Add(movimiento);
                            }
                            else
                            {
                                context.MovimientosDeCaja.Attach(movimiento);
                                context.Entry(movimiento).State = EntityState.Modified;
                            }
                            context.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            throw new ApplicationException(ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene las ventas para la fecha especificada.
        /// </summary>
        /// <param name="fecha">Fecha de venta.</param>
        /// <returns>
        /// Devuelve una lista de ventas para la fecha especificada.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public List<MovimientoDeCaja> ObtenerVentas(DateTime fecha)
        {
            try
            {
                using (var context = ObtenerDbContext())
                {
                    return context.MovimientosDeCaja.Include(m => m.TipoMovimiento)
                                                    .Where(m => DbFunctions.TruncateTime(m.Fecha) == DbFunctions.TruncateTime(fecha) && m.IsDeleted == false)
                                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
    }
}
