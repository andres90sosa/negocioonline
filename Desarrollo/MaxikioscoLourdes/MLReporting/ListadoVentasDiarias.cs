﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLDomain;
using MLContract;
using MLService;
using System.Data;
using System.IO;

namespace MLReporting
{
    /// <summary>
    /// Esta clase permite generar un informe de ventas diarias para una determinada fecha.
    /// </summary>
    public class ListadoVentasDiarias
    {
        private DataSets.DSListadoVentasDiarias ds;
        private DateTime fechaVenta;
        private ReportViewer rv;


        /// <summary>
        /// Obtiene la cantidad de movimientos.
        /// </summary>
        /// <value>Cantidad de movimientos.</value>
        public int CountMovimientos { get; private set; }


        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="ListadoVentasDiarias"/>.
        /// </summary>
        /// <param name="fechaVenta">Fecha de venta para la cual se genera el informe.</param>
        /// <param name="rv">Report Viewer.</param>
        public ListadoVentasDiarias(DateTime fechaVenta, ReportViewer rv)
        {
            this.fechaVenta = fechaVenta;
            this.rv = rv;
            ds = null;
            CountMovimientos = 0;
        }


        /// <summary>
        /// Carga y muestra un informe de ventas diarias para la fecha especificada.
        /// </summary>
        /// <exception cref="System.ApplicationException"></exception>
        public void GenerarInforme()
        {
            List<MovimientoDeCaja> listMovimientos;
            int countNotaCredito = 0;
            decimal totalDiario = 0;
            try
            {
                IServicioMovimientoDeCaja servicioMovimiento = new ServicioMovimientoDeCaja();
                listMovimientos = servicioMovimiento.ObtenerVentas(fechaVenta);

                if(listMovimientos.Count > 0)
                {
                    ds = new DataSets.DSListadoVentasDiarias();
                    CountMovimientos = listMovimientos.Count;

                    foreach (var movimiento in listMovimientos)
                    {
                        CargarMovimientoDeCaja(movimiento);

                        if (movimiento.IdTipoMovimiento == 2)
                        {
                            countNotaCredito++;
                            totalDiario -= movimiento.Total;
                        }
                        else
                            totalDiario += movimiento.Total;
                    }

                    CargarOtrosDatos(fechaVenta, totalDiario, countNotaCredito);
                    this.rv.LocalReport.DataSources.Clear();
                    
                    foreach (DataTable dt in ds.Tables)
                    {
                        ReportDataSource rds = new ReportDataSource();
                        rds.Value = dt;
                        rds.Name = "DS" + dt.TableName;
                        this.rv.LocalReport.DataSources.Add(rds);                        
                    }

                    this.rv.RefreshReport();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }
        }
        
        /// <summary>
        /// Carga los datos de un movimiento de caja en un dataset.
        /// </summary>
        /// <param name="m"> Objeto Movimiento de Caja con el que se completaran los datos en el dataset.</param>
        /// <exception cref="System.ApplicationException"></exception>
        private void CargarMovimientoDeCaja(MovimientoDeCaja m)
        {
            DataSets.DSListadoVentasDiarias.MovimientosDeCajaRow drMovimiento;
            try
            {
                drMovimiento = ds.MovimientosDeCaja.NewMovimientosDeCajaRow();
                drMovimiento.Total = m.IdTipoMovimiento == 2 ? "-" + m.Total.ToString() : m.Total.ToString();
                drMovimiento.TipoMovimiento = m.TipoMovimiento.Nombre;
                ds.MovimientosDeCaja.Rows.Add(drMovimiento);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Carga otros datos en un dataset.
        /// </summary>
        /// <param name="fechaVenta"> Fecha de venta.</param>
        /// <param name="totalDiario">Total diario.</param>
        /// <param name="cantidadNotaCredito">Cantidad de notas de crédito.</param>
        /// <exception cref="System.ApplicationException"></exception>
        private void CargarOtrosDatos(DateTime fechaVenta,decimal totalDiario, int cantidadNotaCredito)
        {
            DataSets.DSListadoVentasDiarias.OtrosDatosRow drOtrosDatos;
            try
            {
                drOtrosDatos = ds.OtrosDatos.NewOtrosDatosRow();
                drOtrosDatos.Fecha = fechaVenta.ToShortDateString();
                drOtrosDatos.CantidadVentas = (ds.MovimientosDeCaja.Rows.Count - (cantidadNotaCredito * 2)).ToString();
                drOtrosDatos.TotalDiario = "$ " + totalDiario.ToString();
                ds.OtrosDatos.Rows.Add(drOtrosDatos);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
