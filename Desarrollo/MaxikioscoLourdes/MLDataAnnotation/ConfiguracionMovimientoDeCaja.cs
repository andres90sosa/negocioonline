﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MLDomain;

namespace MLDataAnnotation
{
    public class ConfiguracionMovimientoDeCaja:EntityTypeConfiguration<MovimientoDeCaja>
    {
        public ConfiguracionMovimientoDeCaja()
        {
            ToTable("MovimientosDeCaja");
            Property(mc => mc.Id).HasColumnName("IdMovimiento");
            HasKey(mc => mc.Id);
            Property(mc => mc.Id).IsRequired();
            Property(mc => mc.Fecha).IsRequired();
            Property(mc => mc.Total).IsRequired();
            Property(mc => mc.IsDeleted).IsRequired();
            Property(mc => mc.IdTipoMovimiento).IsRequired().HasColumnOrder(5);
            Property(mc => mc.IsDeleted).HasColumnOrder(4).HasColumnType("bit");
            HasMany(mc => mc.DetallesMovimientoDeCaja).WithRequired(d => d.MovimientoDeCaja);
            HasRequired(mc => mc.TipoMovimiento).WithMany().HasForeignKey(mc => mc.IdTipoMovimiento);
        }
    }
}
