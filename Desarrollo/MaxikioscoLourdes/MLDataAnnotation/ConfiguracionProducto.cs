﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MLDomain;

namespace MLDataAnnotation
{
    public class ConfiguracionProducto: EntityTypeConfiguration<Producto>
    {
        public ConfiguracionProducto()
        {
            ToTable("Productos");
            Property(p => p.Id).HasColumnName("IdProducto");
            HasKey(p => p.Id);
            Property(p => p.Nombre).HasColumnName("NombreProducto");
            Property(p => p.PrecioCosto).IsOptional();
            Property(p => p.PrecioVenta).IsOptional();
            Property(p => p.PorcentajeGanancia).IsOptional();
            Property(p => p.CodigoBarra).IsOptional();
            Property(p => p.Imagen).IsOptional();
            Property(p => p.IsDeleted).HasColumnOrder(11).HasColumnType("bit");
            Property(p => p.Id).IsRequired();
            Property(p => p.Nombre).IsRequired();
            Property(p => p.IsDeleted).IsRequired();
            Property(p => p.CodNegocio).IsRequired();
            Property(p => p.Nombre).HasMaxLength(100);
            Property(p => p.CodigoBarra).HasMaxLength(100);
            Property(p => p.PrecioCosto).HasPrecision(10, 2);
            Property(p => p.PrecioVenta).HasPrecision(10, 2);
            Property(p => p.PorcentajeGanancia).HasPrecision(5, 2);
            HasRequired(p => p.Tipo).WithMany().HasForeignKey(p => p.IdTipoProd);
            HasOptional(p => p.Marca).WithMany().HasForeignKey(p => p.IdMarcaProd);
        }
    }
}
