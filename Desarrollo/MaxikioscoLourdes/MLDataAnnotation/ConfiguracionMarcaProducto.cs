﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MLDomain;

namespace MLDataAnnotation
{
    public class ConfiguracionMarcaProducto:EntityTypeConfiguration<MarcaProducto>
    {
        public ConfiguracionMarcaProducto()
        {
            ToTable("MarcasProducto");
            Property(mp => mp.Id).HasColumnName("IdMarcaProd");
            HasKey(mp => mp.Id);
            Property(mp => mp.Id).IsRequired();
            Property(mp => mp.Nombre).IsRequired().HasColumnName("NombreMarcaProd");
            Property(mp => mp.CodNegocio).IsRequired();
            Property(mp => mp.IsDeleted).IsRequired();
            Property(mp => mp.Nombre).HasMaxLength(100);
            Property(mp => mp.IsDeleted).HasColumnType("bit");
            Property(mp => mp.IsDeleted).HasColumnOrder(4);
            Property(mp => mp.FechaCreacion).IsRequired();
            Property(mp => mp.FechaModificacion).IsOptional();
        }
    }
}
