﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MLDomain;

namespace MLDataAnnotation
{
    public class ConfiguracionTipoProducto:EntityTypeConfiguration<TipoProducto>
    {
        public ConfiguracionTipoProducto()
        {
            ToTable("TiposProducto");
            Property(tp => tp.Id).HasColumnName("IdTipoProd");
            HasKey(tp => tp.Id);
            Property(tp => tp.Id).IsRequired();
            Property(tp => tp.Nombre).IsRequired().HasColumnName("NombreTipoProd");
            Property(tp => tp.Nivel).IsRequired();
            Property(tp => tp.CodNegocio).IsRequired();
            Property(tp => tp.IsDeleted).IsRequired().HasColumnOrder(6).HasColumnType("bit");
            Property(tp => tp.Nombre).HasMaxLength(100);
            HasOptional(tp => tp.TipoDeProductoPadre).WithMany().HasForeignKey(tp => tp.IdTipoProdPadre);
        }
    }
}
