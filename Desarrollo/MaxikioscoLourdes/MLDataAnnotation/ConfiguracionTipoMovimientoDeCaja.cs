﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MLDomain;

namespace MLDataAnnotation
{
    public class ConfiguracionTipoMovimientoDeCaja: EntityTypeConfiguration<TipoMovimientoDeCaja>
    {
        public ConfiguracionTipoMovimientoDeCaja()
        {
            ToTable("TiposMovimientoDeCaja");
            Property(tm => tm.Id).HasColumnName("IdTipoMovimiento");
            HasKey(tm => tm.Id);
            Property(tm => tm.Id).IsRequired();
            Property(tm => tm.Nombre).IsRequired();
            Property(tm => tm.IsDeleted).IsRequired();
            Property(tm => tm.Nombre).HasMaxLength(50);
            Property(tm => tm.IsDeleted).HasColumnOrder(3).HasColumnType("bit");
        }
    }
}
