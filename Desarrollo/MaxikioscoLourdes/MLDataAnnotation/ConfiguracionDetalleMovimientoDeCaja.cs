﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using MLDomain;

namespace MLDataAnnotation
{
    public class ConfiguracionDetalleMovimientoDeCaja:EntityTypeConfiguration<DetalleMovimientoDeCaja>
    {
        public ConfiguracionDetalleMovimientoDeCaja()
        {
            ToTable("DetallesMovimientosDeCaja");
            Property(d => d.Id).HasColumnName("IdDetalleMovimiento");
            HasKey(d => d.Id);
            Property(d => d.Id).IsRequired();
            Property(d => d.IdMovimiento).IsRequired();
            Property(d => d.IdProducto).IsRequired();
            Property(d => d.Cantidad).IsRequired();
            Property(d => d.PrecioVentaUnitario).IsRequired();
            Property(d => d.IsDeleted).IsRequired();
            Property(d => d.PrecioVentaUnitario).HasPrecision(6, 2);
            Property(d => d.IsDeleted).HasColumnOrder(6).HasColumnType("bit");
            HasRequired(d => d.Producto).WithMany().HasForeignKey(d => d.IdProducto);
            HasRequired(d => d.MovimientoDeCaja).WithMany(v => v.DetallesMovimientoDeCaja).HasForeignKey(d => d.IdMovimiento);
            Ignore(d => d.Subtotal);
        }
    }
}
