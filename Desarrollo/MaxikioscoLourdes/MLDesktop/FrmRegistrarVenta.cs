﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MLContract;
using MLService;
using MLDomain;
using System.Text.RegularExpressions;

namespace MLDesktop
{
    public partial class FrmRegistrarVenta : Form
    {
        private IServicioProducto servicioProducto;
        private MovimientoDeCaja movimiento;
        private bool actualizarDetalle;
        Producto producto;


        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="FrmRegistrarVenta"/>.
        /// </summary>
        public FrmRegistrarVenta()
        {   
            InitializeComponent();
        }

        #region Eventos

        /// <summary>
        /// Maneja el evento cargar del formulario FrmRegistrarVenta.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/>Instancia que contiene los datos del evento.</param>
        private void FrmRegistrarVenta_Load(object sender, EventArgs e)
        {
            try
            {
                servicioProducto = new ServicioProducto();
                movimiento = new MovimientoDeCaja();
                actualizarDetalle = false;
                CargarToolTips();
                cbActualizarPrecioVenta_CheckedChanged(null, null);
                cbCantidadPesos_CheckedChanged(null, null);
                ucBuscarProducto1.EventClickBtnBuscar += new MLCustomControls.UCBuscarProducto.MyEventClick(CargarProductoSeleccionado);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al cargar formulario", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento Click del botón btnAgregar.
        /// </summary>
        /// <param name="sender">Control que orifina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarDetalleMovimientoEnGrilla();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al agregar detalle de " + ObtenerTipoOperacion() + " a la grilla", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento Click del botón <see cref="btnModificar"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ModoEdicionDetalleMovimientoDeCaja();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al cargar detalle de " + ObtenerTipoOperacion(), MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace doble click en una fila de la grilla <see cref="gvDetalleVenta"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/>Instancia que contiene los datos del evento.</param>
        private void gvDetalleVenta_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModoEdicionDetalleMovimientoDeCaja();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al cargar detalle de " + ObtenerTipoOperacion(), MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento click del botón <see cref="btnEliminar"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                EliminarDetalleMovimientoDeCaja();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al eliminar detalle de " + ObtenerTipoOperacion(), MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se presiona una tecla sobre la grilla <see cref="gvDetalleVenta"/>.
        /// Al presionar la tecla 'Supr' se llama al método <see cref="EliminarDetalleMovimientoDeCaja"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void gvDetalleVenta_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    EliminarDetalleMovimientoDeCaja();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al eliminar detalle de " + ObtenerTipoOperacion(), MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento click del control <see cref="rbVenta"/>. Cambia el texto del panel <see cref="gbDetalleVenta"/> a 'Detalle de Venta'. 
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void rbVenta_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                gbDetalleVenta.Text = "Detalle de Venta";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al hacer click en el tipo de operación venta", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento click del control <see cref="rbNotaDeCredito"/>. Cambia el texto del panel <see cref="gbDetalleVenta"/> a 'Detalle de Nota de Crédito'. 
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void rbNotaDeCredito_Click(object sender, EventArgs e)
        {
            try
            {
                gbDetalleVenta.Text = "Detalle de Nota de Crédito";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al hacer click en el tipo de operación Nota de Crédito", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento click del botón <see cref="btnCancelar"/>.
        /// Solicita una confirmación al usuario si desea cancelar la operación y en caso afirmativo inicializa la IU.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Esta seguro que desea cancelar la " + ObtenerTipoOperacion() + "?", "Cancelar " + ObtenerTipoOperacion(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    inicializarIU();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al cancelar la " + ObtenerTipoOperacion(), MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento click del botón <see cref="btnConfirmar"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            
            try
            {
                ConfirmarMovimiento();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al confirmar " + ObtenerTipoOperacion(), MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se tilda o destilda el control <see cref="cbActualizarPrecioVenta"/>. 
        /// Cuando el control se tilda habilita el campo de texto <see cref="txtPrecioVentaProducto"/> y hace foco sobre el mismo.
        /// Cuando el control se destilda deshabilita el campo de texto <see cref="txtPrecioVentaProducto"/> y seta el texto con una cadena vacia 
        /// o con el precio de venta obtenido por BD.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void cbActualizarPrecioVenta_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbActualizarPrecioVenta.Checked)
                {
                    txtPrecioVentaProducto.Enabled = true;
                    txtPrecioVentaProducto.Focus();
                    txtPrecioVentaProducto.SelectionStart = txtPrecioVentaProducto.Text.Length;
                }
                else
                {
                    txtPrecioVentaProducto.Enabled = false;

                    if (this.producto != null)
                        txtPrecioVentaProducto.Text = this.producto.PrecioVenta.ToString();
                    else
                        txtPrecioVentaProducto.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al tildar opción actualizar", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se tilda o destilda el checkbox <see cref="cbCantidadPesos"/>.Cuando el control se tilda 
        /// habilita el campo de texto <see cref="txtCantidadPesos"/> y deshabilita el campo de texto <see cref="txtCantidad"/>.
        /// Cuando el control se destilda deshabilita el campo de texto <see cref="txtCantidadPesos"/> y habilita el campo de texto <see cref="txtCantidad"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void cbCantidadPesos_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbCantidadPesos.Checked)
                {
                    txtCantidadPesos.Enabled = true;
                    txtCantidadPesos.Focus();
                    txtCantidad.Enabled = false;
                }
                else
                {
                    txtCantidadPesos.Enabled = false;
                    txtCantidadPesos.Text = "";
                    txtCantidad.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al tildar la opción cantidad en pesos", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se presiona una tecla en el campo txtCantidad. 
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtCantidad_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    GuardarDetalleMovimientoEnGrilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al agregar detalle de " + ObtenerTipoOperacion() + " a la grilla", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }


        /// <summary>
        /// Maneja el evento cuando se presiona una tecla en el campo <see cref="txtCantidadPesos"/>. 
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtCantidadPesos_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    GuardarDetalleMovimientoEnGrilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al agregar detalle de " + ObtenerTipoOperacion() + " a la grilla", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se presiona una tecla en el campo <see cref="txtPrecioVentaProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtPrecioVentaProducto_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    GuardarDetalleMovimientoEnGrilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al agregar detalle de " + ObtenerTipoOperacion() + " a la grilla", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se cambia el texto del campo <see cref="txtCantidadPesos"/>
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtCantidadPesos_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalcularCantidadDeUnidadesProducto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al calcular la cantidad de unidades del producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se cambia el texto del campo <see cref="txtPrecioVentaProducto"/>
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtPrecioVentaProducto_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalcularCantidadDeUnidadesProducto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al calcular la cantidad de unidades del producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        ///Maneja el evento cuando se cambia el texto del campo <see cref="txtPagaCon"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void txtPagaCon_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalcularVuelto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al calcular vuelto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        #endregion

        #region MetodosDelFormulario

        /// <summary>
        /// Carga los datos del producto seleccionado en los campos de texto.
        /// </summary>
        ///  /// <exception cref="ApplicationException"></exception>
        private void CargarProductoSeleccionado()
        {
            try
            {
                this.producto = ucBuscarProducto1.ProductoSeleccionado;

                if (this.producto != null)
                {
                    inicializarCamposSeccionBuscarProducto();
                    txtIdProducto.Text = this.producto.Id.ToString();
                    txtNombreProducto.Text = this.producto.Nombre;
                    txtPrecioVentaProducto.Text = this.producto.PrecioVenta.ToString();
                    txtCantidad.Focus();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Carga los toolTips de campos de texto y botones.
        /// </summary>
        private void CargarToolTips()
        {
            //ToolTip confirmar
            ToolTip toolTipConfirmar = new ToolTip();
            toolTipConfirmar.ShowAlways = true;
            toolTipConfirmar.SetToolTip(btnConfirmar, "Haga click aquí para confirmar la operación.");

            //ToolTip cancelar
            ToolTip toolTipCancelar = new ToolTip();
            toolTipCancelar.ShowAlways = true;
            toolTipCancelar.SetToolTip(btnCancelar, "Haga click aquí para cancelar la operación.");

            //ToolTip agregar producto
            ToolTip toolTipAgregar = new ToolTip();
            toolTipAgregar.ShowAlways = true;
            toolTipAgregar.SetToolTip(btnAgregar, "Haga click aquí para agregar un producto.");

            //ToolTip modificar producto
            ToolTip toolTipModificar = new ToolTip();
            toolTipModificar.ShowAlways = true;
            toolTipModificar.SetToolTip(btnModificar, "Haga click aquí para modificar un producto de la grilla.");

            //ToolTip eliminar producto
            ToolTip toolTipEliminar = new ToolTip();
            toolTipEliminar.ShowAlways = true;
            toolTipEliminar.SetToolTip(btnEliminar, "Haga click aquí para eliminar un producto de la grilla.");

            //ToolTip ingresar cantidad
            ToolTip toolTipIngresarCant = new ToolTip();
            toolTipIngresarCant.ShowAlways = true;
            toolTipIngresarCant.SetToolTip(txtCantidad, "Ingrese cantidad del producto.");

            //Tooltip precio de venta del producto
            ToolTip toolTipPrecioVenta = new ToolTip();
            toolTipPrecioVenta.ShowAlways = false;
            toolTipPrecioVenta.SetToolTip(txtPrecioVentaProducto, "Ingrese el precio de venta del producto.");

            ToolTip toolTipCantidadPesos = new ToolTip();
            toolTipCantidadPesos.ShowAlways = false;
            toolTipCantidadPesos.SetToolTip(txtCantidadPesos, "Ingrese la cantidad del producto expresada en pesos ($).");
        }

        /// <summary>
        /// Obtiene el tipo de operación a realizar (venta o nota de crédito).
        /// </summary>
        /// <returns>Retorna el tipo de operación a realizar.</returns>
        /// <exception cref="ApplicationException"></exception>
        private string ObtenerTipoOperacion()
        {
            try
            {
                if (rbVenta.Checked)
                    return "venta";
                else
                    return "nota de crédito";
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Inicializa los campos de texto de la sección 'Buscar Producto'.
        /// </summary>
        private void inicializarCamposSeccionBuscarProducto()
        {
            ucBuscarProducto1.ClearTextBoxProducto();
            ucBuscarProducto1.EnabledTextBox(true);
            ucBuscarProducto1.EnabledBotonBuscar(true);
            txtIdProducto.Text = string.Empty;
            txtNombreProducto.Text = string.Empty;
            txtPrecioVentaProducto.Text = string.Empty;
            txtCantidad.Value = 1;
            txtCantidad.Enabled = true;
            cbActualizarPrecioVenta.Checked = false;
            cbCantidadPesos.Checked = false;
            ucBuscarProducto1.FocusTextBoxProducto();
        }


        /// <summary>
        /// Valida y guarda un detalle de movimiento de caja en la grilla <see cref= "gvDetalleVenta" />.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void GuardarDetalleMovimientoEnGrilla()
        {
            int cantidad = 0;
            decimal precioVenta = 0;
            DetalleMovimientoDeCaja detalle;
            try
            {
                if (ValidarDetalleMovimientoDeCaja())
                {
                    detalle = movimiento.DetallesMovimientoDeCaja.FirstOrDefault(d => d.IdProducto == int.Parse(txtIdProducto.Text));
                    cantidad = int.Parse(txtCantidad.Value.ToString());
                    precioVenta = decimal.Parse(txtPrecioVentaProducto.Text.Trim());

                    if (actualizarDetalle)
                    {
                        //actualizar detalle
                        detalle.Cantidad = cantidad;
                        detalle.PrecioVentaUnitario = precioVenta;
                        actualizarDetalle = false;
                    }
                    else
                    {
                        //nuevo detalle
                        if (detalle != null)
                        {
                            //El producto que se agrega como detalle ya existe en la grilla
                            detalle.Cantidad += cantidad;

                            if (precioVenta != detalle.Producto.PrecioVenta)
                                detalle.PrecioVentaUnitario = precioVenta;
                        }
                        else
                        {
                            //El producto que se agrega como detalle NO existe en la grilla
                            detalle = new DetalleMovimientoDeCaja();
                            detalle.PrecioVentaUnitario = precioVenta;
                            detalle.Cantidad = cantidad;
                            detalle.Producto = this.producto;
                            detalle.IdProducto = this.producto.Id;
                            movimiento.DetallesMovimientoDeCaja.Add(detalle);
                        }

                        //Habilitamos los botones modificar y eliminar
                        if (!btnModificar.Visible && !btnEliminar.Visible)
                        {
                            btnModificar.Visible = true;
                            btnEliminar.Visible = true;
                        }
                    }
                    
                    gvDetalleVenta.DataSource = movimiento.ObtenerDatosDetallesMovimientoDeCaja();
                    movimiento.CalcularTotal();
                    txtTotal.Text = movimiento.Total.ToString();
                    CalcularVuelto();
                    this.producto = null;
                    inicializarCamposSeccionBuscarProducto();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
        /// <summary>
        /// Valida los datos de un detalle de movimiento de caja.
        /// </summary>
        /// <returns>Retorna <c>true</c> si el detalle es válido y <c>false</c> en caso contrario.</returns>
        /// <exception cref="ApplicationException">
        /// </exception>
        private bool ValidarDetalleMovimientoDeCaja()
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombreProducto.Text.Trim()))
                {
                    MessageBox.Show("No ingresó el producto que desea agregar al detalle. Busque el producto que desea agregar al detalle e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    ucBuscarProducto1.FocusTextBoxProducto();
                    return false;
                }
                if (string.IsNullOrEmpty(((Control)this.txtCantidad).Text))
                {
                    MessageBox.Show("Ingrese la cantidad del producto.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    txtCantidad.Focus();
                    return false;
                }

                if (txtPrecioVentaProducto.Enabled && string.IsNullOrEmpty(txtPrecioVentaProducto.Text.Trim()))
                {
                    MessageBox.Show("Ingrese el precio de venta del producto.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    txtPrecioVentaProducto.Focus();
                    return false;
                }
                else if (!string.IsNullOrEmpty(txtPrecioVentaProducto.Text.Trim()))
                    if (decimal.Parse(txtPrecioVentaProducto.Text.Trim()) <= 0)
                    {
                        MessageBox.Show("El precio de venta del producto debe ser superior a cero (0).", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                        txtPrecioVentaProducto.Focus();
                        txtPrecioVentaProducto.SelectionStart = txtPrecioVentaProducto.Text.Length;
                        return false;
                    }

                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
        /// <summary>
        /// Carga en los campos de texto de la sección 'Buscar Producto' los datos del detalle de movimiento de caja 
        /// seleccionado en la grilla para poder editarlo y se deshabilita el user control <see cref="ucBuscarProducto1"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void ModoEdicionDetalleMovimientoDeCaja()
        {
            decimal precioVenta = 0;
            decimal? precioVentaAux = 0;
            int idProducto = 0;
            DetalleMovimientoDeCaja detalle;
            try
            {
                if (actualizarDetalle)
                {
                    MessageBox.Show("No es posible editar el detalle de " + ObtenerTipoOperacion() + " porque otro detalle esta siendo editado. Confirme la edición del detalle e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return;
                }

                if (ValidarSeleccionDetalleMovimientoDeCaja())
                {
                    ucBuscarProducto1.EnabledTextBox(false);
                    ucBuscarProducto1.EnabledBotonBuscar(false);
                    actualizarDetalle = true;
                    idProducto = int.Parse(gvDetalleVenta.SelectedRows[0].Cells["IdProducto"].Value.ToString());
                    detalle = movimiento.DetallesMovimientoDeCaja.FirstOrDefault(d => d.IdProducto == idProducto);
                    txtIdProducto.Text = detalle.Producto.Id.ToString();
                    txtNombreProducto.Text = detalle.Producto.Nombre;
                    txtCantidad.Value = detalle.Cantidad;
                    precioVenta = detalle.PrecioVentaUnitario;
                    precioVentaAux = detalle.Producto.PrecioVenta;
                    txtPrecioVentaProducto.Text = precioVenta.ToString();
                    this.producto = detalle.Producto;

                    if (precioVenta != precioVentaAux)
                        cbActualizarPrecioVenta.Checked = true;

                    txtCantidad.Focus();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Elimina un detalle de movimiento de caja de la grilla <see cref="gvDetalleVenta"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void EliminarDetalleMovimientoDeCaja()
        {
            DetalleMovimientoDeCaja detalle;
            decimal total = 0;
            try
            {
                if (actualizarDetalle)
                {
                    MessageBox.Show("No es posible eliminar el detalle de " + ObtenerTipoOperacion() + " porque esta siendo editado. Confirme la edición del detalle e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return;
                }

                if (ValidarSeleccionDetalleMovimientoDeCaja())
                {
                    detalle = movimiento.DetallesMovimientoDeCaja.FirstOrDefault(d => d.IdProducto == int.Parse(gvDetalleVenta.SelectedRows[0].Cells["IdProducto"].Value.ToString()));
                    movimiento.DetallesMovimientoDeCaja.Remove(detalle);
                    gvDetalleVenta.DataSource = movimiento.ObtenerDatosDetallesMovimientoDeCaja();
                    movimiento.CalcularTotal();
                    total = movimiento.Total;

                    if (total > 0)
                        txtTotal.Text = total.ToString();
                    else
                        txtTotal.Text = "";

                    CalcularVuelto();
                    ucBuscarProducto1.FocusTextBoxProducto();

                    // Si ya no quedan mas elementos para eliminar ocultamos los botones de edición y eliminación.
                    if (gvDetalleVenta.RowCount <= 0)
                    {
                        btnModificar.Visible = false;
                        btnEliminar.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Valida que se seleccione un detalle de movimiento de caja de la grilla <see cref="gvDetalleVenta"/> para poder editarlo o eliminarlo.
        /// </summary>
        /// <returns>Retorna <c>true</c> si se seleccionó un detalle de la grilla y <c>false</c> en caso contrario.</returns>
        /// <exception cref="ApplicationException">
        /// </exception>
        private bool ValidarSeleccionDetalleMovimientoDeCaja()
        {
            try
            {
                if (gvDetalleVenta.SelectedRows.Count <= 0)
                {
                    MessageBox.Show("Seleccione un detalle de " + ObtenerTipoOperacion() + " de la grilla para poder editarlo o eliminarlo.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Calcula la cantidad de unidades de un producto en base a la cantidad en pesos  y al precio de venta del producto y setea el campo <see cref="txtCantidad"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void CalcularCantidadDeUnidadesProducto()
        {
            int cantidadDeUnidades = 1;
            decimal cantidadEnPesos = 0;
            decimal? precioVenta = 0;
            try
            {
                if (txtCantidadPesos.Enabled)
                {
                    if (!string.IsNullOrEmpty(txtCantidadPesos.Text.Trim()))
                    {
                        if (!string.IsNullOrEmpty(txtPrecioVentaProducto.Text.Trim()))
                        {
                            precioVenta = decimal.Parse(txtPrecioVentaProducto.Text.Trim());
                            cantidadEnPesos = decimal.Parse(txtCantidadPesos.Text.Trim());

                            if (precioVenta > 0 && cantidadEnPesos > 0 && cantidadEnPesos >= precioVenta)
                            {
                                cantidadDeUnidades = (int)(cantidadEnPesos / precioVenta);

                                if (cantidadDeUnidades > 100)
                                {
                                    MessageBox.Show("La cantidad de unidades del producto no puede ser superior a 100. Ingrese una cantidad en pesos menor.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                                    return;
                                }
                            }

                        }
                    }
                    txtCantidad.Value = cantidadDeUnidades;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Inicializa la interfaz de usuario.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void inicializarIU()
        {
            try
            {
                this.producto = null;
                rbVenta.Checked = true;
                movimiento = new MovimientoDeCaja();
                actualizarDetalle = false;
                btnModificar.Visible = false;
                btnEliminar.Visible = false;
                txtTotal.Text = string.Empty;
                txtPagaCon.Text = string.Empty;
                txtVuelto.Text = string.Empty;
                gvDetalleVenta.DataSource = movimiento.ObtenerDatosDetallesMovimientoDeCaja();
                inicializarCamposSeccionBuscarProducto();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Calcula y muestra el vuelto del cliente.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void CalcularVuelto()
        {
            decimal vuelto = 0;
            decimal pagaCon = 0;
            decimal total = 0;
            try
            {
                if (!string.IsNullOrEmpty(txtTotal.Text) && !string.IsNullOrEmpty(txtPagaCon.Text))
                {
                    if (decimal.TryParse(txtPagaCon.Text,out decimal res))
                    {
                        pagaCon = decimal.Parse(txtPagaCon.Text);
                        total = decimal.Parse(txtTotal.Text);

                        if (pagaCon >= total)
                        {
                            vuelto = pagaCon - total;
                            txtVuelto.Text = vuelto.ToString();
                        }
                        else
                            txtVuelto.Text = string.Empty;
                    }
                }
                else
                    txtVuelto.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        /// <summary>
        /// Valida la confirmación de un movimiento de caja. Valida que al menos haya un detalle en la grilla.
        /// </summary>
        /// <returns>
        /// Devuelve <c>true</c> si la grilla tiene uno o mas detalles y <c>false</c> en caso contrario.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// </exception>
        private bool ValidarConfirmarOperacion()
        {
            try
            {
                if (gvDetalleVenta.Rows.Count <= 0)
                {
                    MessageBox.Show("La " + ObtenerTipoOperacion() + " debe tener al menos un detalle.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Registra la venta o nota de crédito con su correspondiente detalle.
        /// </summary>
        private void ConfirmarMovimiento()
        {
            string nombreTipoMovimiento = string.Empty;
            try
            {
                if (ValidarConfirmarOperacion())
                {
                    nombreTipoMovimiento = ObtenerTipoOperacion();
                    movimiento.Fecha = DateTime.Now;

                    if (rbVenta.Checked)
                        movimiento.IdTipoMovimiento = 1;
                    else
                        movimiento.IdTipoMovimiento = 2;

                    IServicio<MovimientoDeCaja> servicioMovimiento = new ServicioMovimientoDeCaja();
                    servicioMovimiento.Guardar(movimiento);
                    inicializarIU();
                    MessageBox.Show("La " + nombreTipoMovimiento + " se registro correctamente!!!", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        #endregion

    }
}
