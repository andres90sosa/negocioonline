﻿namespace MLDesktop
{
    partial class FrmRegistrarVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrarVenta));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbProductos = new System.Windows.Forms.GroupBox();
            this.txtIdProducto = new System.Windows.Forms.TextBox();
            this.lblIdProducto = new System.Windows.Forms.Label();
            this.lblPrecioVentaProducto = new System.Windows.Forms.Label();
            this.lblNombreProducto = new System.Windows.Forms.Label();
            this.txtNombreProducto = new System.Windows.Forms.TextBox();
            this.cbActualizarPrecioVenta = new System.Windows.Forms.CheckBox();
            this.cbCantidadPesos = new System.Windows.Forms.CheckBox();
            this.txtCantidad = new System.Windows.Forms.NumericUpDown();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.gvDetalleVenta = new System.Windows.Forms.DataGridView();
            this.gbDetalleVenta = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtVuelto = new System.Windows.Forms.TextBox();
            this.gbTotal = new System.Windows.Forms.GroupBox();
            this.lblPagaCon = new System.Windows.Forms.Label();
            this.lblVuelto = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.lblOperacionARealizar = new System.Windows.Forms.Label();
            this.rbVenta = new System.Windows.Forms.RadioButton();
            this.rbNotaDeCredito = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblOperacionARealizar2 = new System.Windows.Forms.Label();
            this.barraDeAcciones = new System.Windows.Forms.Panel();
            this.ucBuscarProducto1 = new MLCustomControls.UCBuscarProducto();
            this.txtPagaCon = new MLCustomControls.DecimalTextBox();
            this.txtCantidadPesos = new MLCustomControls.DecimalTextBox();
            this.txtPrecioVentaProducto = new MLCustomControls.DecimalTextBox();
            this.Product = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioVentaAux = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbProductos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetalleVenta)).BeginInit();
            this.gbDetalleVenta.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.gbTotal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.barraDeAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbProductos
            // 
            this.gbProductos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProductos.BackColor = System.Drawing.SystemColors.Control;
            this.gbProductos.Controls.Add(this.txtIdProducto);
            this.gbProductos.Controls.Add(this.lblIdProducto);
            this.gbProductos.Controls.Add(this.lblPrecioVentaProducto);
            this.gbProductos.Controls.Add(this.lblNombreProducto);
            this.gbProductos.Controls.Add(this.txtNombreProducto);
            this.gbProductos.Controls.Add(this.cbActualizarPrecioVenta);
            this.gbProductos.Controls.Add(this.txtCantidadPesos);
            this.gbProductos.Controls.Add(this.txtPrecioVentaProducto);
            this.gbProductos.Controls.Add(this.cbCantidadPesos);
            this.gbProductos.Controls.Add(this.txtCantidad);
            this.gbProductos.Controls.Add(this.lblCantidad);
            this.gbProductos.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProductos.ForeColor = System.Drawing.Color.Black;
            this.gbProductos.Location = new System.Drawing.Point(176, 74);
            this.gbProductos.Name = "gbProductos";
            this.gbProductos.Size = new System.Drawing.Size(716, 162);
            this.gbProductos.TabIndex = 0;
            this.gbProductos.TabStop = false;
            this.gbProductos.Text = "Buscar Producto";
            // 
            // txtIdProducto
            // 
            this.txtIdProducto.Enabled = false;
            this.txtIdProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdProducto.Location = new System.Drawing.Point(94, 84);
            this.txtIdProducto.Name = "txtIdProducto";
            this.txtIdProducto.Size = new System.Drawing.Size(80, 27);
            this.txtIdProducto.TabIndex = 36;
            // 
            // lblIdProducto
            // 
            this.lblIdProducto.AutoSize = true;
            this.lblIdProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdProducto.ForeColor = System.Drawing.Color.Black;
            this.lblIdProducto.Location = new System.Drawing.Point(6, 87);
            this.lblIdProducto.Name = "lblIdProducto";
            this.lblIdProducto.Size = new System.Drawing.Size(30, 21);
            this.lblIdProducto.TabIndex = 35;
            this.lblIdProducto.Text = "Id:";
            // 
            // lblPrecioVentaProducto
            // 
            this.lblPrecioVentaProducto.AutoSize = true;
            this.lblPrecioVentaProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioVentaProducto.ForeColor = System.Drawing.Color.Black;
            this.lblPrecioVentaProducto.Location = new System.Drawing.Point(6, 129);
            this.lblPrecioVentaProducto.Name = "lblPrecioVentaProducto";
            this.lblPrecioVentaProducto.Size = new System.Drawing.Size(61, 21);
            this.lblPrecioVentaProducto.TabIndex = 34;
            this.lblPrecioVentaProducto.Text = "Precio:";
            // 
            // lblNombreProducto
            // 
            this.lblNombreProducto.AutoSize = true;
            this.lblNombreProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreProducto.ForeColor = System.Drawing.Color.Black;
            this.lblNombreProducto.Location = new System.Drawing.Point(194, 87);
            this.lblNombreProducto.Name = "lblNombreProducto";
            this.lblNombreProducto.Size = new System.Drawing.Size(77, 21);
            this.lblNombreProducto.TabIndex = 33;
            this.lblNombreProducto.Text = "Nombre:";
            // 
            // txtNombreProducto
            // 
            this.txtNombreProducto.Enabled = false;
            this.txtNombreProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreProducto.Location = new System.Drawing.Point(277, 84);
            this.txtNombreProducto.Name = "txtNombreProducto";
            this.txtNombreProducto.Size = new System.Drawing.Size(422, 27);
            this.txtNombreProducto.TabIndex = 32;
            // 
            // cbActualizarPrecioVenta
            // 
            this.cbActualizarPrecioVenta.AutoSize = true;
            this.cbActualizarPrecioVenta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbActualizarPrecioVenta.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbActualizarPrecioVenta.ForeColor = System.Drawing.Color.Black;
            this.cbActualizarPrecioVenta.Location = new System.Drawing.Point(180, 133);
            this.cbActualizarPrecioVenta.Name = "cbActualizarPrecioVenta";
            this.cbActualizarPrecioVenta.Size = new System.Drawing.Size(82, 17);
            this.cbActualizarPrecioVenta.TabIndex = 22;
            this.cbActualizarPrecioVenta.Text = "Actualizar";
            this.cbActualizarPrecioVenta.UseVisualStyleBackColor = true;
            this.cbActualizarPrecioVenta.CheckedChanged += new System.EventHandler(this.cbActualizarPrecioVenta_CheckedChanged);
            // 
            // cbCantidadPesos
            // 
            this.cbCantidadPesos.AutoSize = true;
            this.cbCantidadPesos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbCantidadPesos.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCantidadPesos.ForeColor = System.Drawing.Color.Black;
            this.cbCantidadPesos.Location = new System.Drawing.Point(534, 131);
            this.cbCantidadPesos.Name = "cbCantidadPesos";
            this.cbCantidadPesos.Size = new System.Drawing.Size(153, 17);
            this.cbCantidadPesos.TabIndex = 24;
            this.cbCantidadPesos.Text = "Cantidad en pesos ($)";
            this.cbCantidadPesos.UseVisualStyleBackColor = true;
            this.cbCantidadPesos.CheckedChanged += new System.EventHandler(this.cbCantidadPesos_CheckedChanged);
            // 
            // txtCantidad
            // 
            this.txtCantidad.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidad.Location = new System.Drawing.Point(366, 126);
            this.txtCantidad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(76, 27);
            this.txtCantidad.TabIndex = 21;
            this.txtCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtCantidad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCantidad_KeyDown);
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.ForeColor = System.Drawing.Color.Black;
            this.lblCantidad.Location = new System.Drawing.Point(273, 128);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(87, 21);
            this.lblCantidad.TabIndex = 14;
            this.lblCantidad.Text = "Cantidad";
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAgregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnAgregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ForeColor = System.Drawing.Color.White;
            this.btnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregar.Image")));
            this.btnAgregar.Location = new System.Drawing.Point(290, 0);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(140, 57);
            this.btnAgregar.TabIndex = 11;
            this.btnAgregar.Text = " Agregar   Detalle";
            this.btnAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // gvDetalleVenta
            // 
            this.gvDetalleVenta.AllowUserToAddRows = false;
            this.gvDetalleVenta.AllowUserToDeleteRows = false;
            this.gvDetalleVenta.AllowUserToResizeColumns = false;
            this.gvDetalleVenta.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.gvDetalleVenta.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gvDetalleVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvDetalleVenta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvDetalleVenta.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.gvDetalleVenta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gvDetalleVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvDetalleVenta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Product,
            this.Cantidad,
            this.PrecioVenta,
            this.Subtotal,
            this.PrecioVentaAux,
            this.IdProducto});
            this.gvDetalleVenta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gvDetalleVenta.Location = new System.Drawing.Point(0, 71);
            this.gvDetalleVenta.MultiSelect = false;
            this.gvDetalleVenta.Name = "gvDetalleVenta";
            this.gvDetalleVenta.ReadOnly = true;
            this.gvDetalleVenta.RowHeadersVisible = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.gvDetalleVenta.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.gvDetalleVenta.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvDetalleVenta.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            this.gvDetalleVenta.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            this.gvDetalleVenta.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.gvDetalleVenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvDetalleVenta.Size = new System.Drawing.Size(619, 248);
            this.gvDetalleVenta.TabIndex = 1;
            this.gvDetalleVenta.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvDetalleVenta_CellDoubleClick);
            this.gvDetalleVenta.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gvDetalleVenta_KeyUp);
            // 
            // gbDetalleVenta
            // 
            this.gbDetalleVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDetalleVenta.BackColor = System.Drawing.SystemColors.Control;
            this.gbDetalleVenta.Controls.Add(this.flowLayoutPanel1);
            this.gbDetalleVenta.Controls.Add(this.gvDetalleVenta);
            this.gbDetalleVenta.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDetalleVenta.ForeColor = System.Drawing.Color.Black;
            this.gbDetalleVenta.Location = new System.Drawing.Point(5, 242);
            this.gbDetalleVenta.Name = "gbDetalleVenta";
            this.gbDetalleVenta.Size = new System.Drawing.Size(619, 327);
            this.gbDetalleVenta.TabIndex = 2;
            this.gbDetalleVenta.TabStop = false;
            this.gbDetalleVenta.Text = "Detalle de Venta";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.flowLayoutPanel1.Controls.Add(this.btnModificar);
            this.flowLayoutPanel1.Controls.Add(this.btnEliminar);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 21);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(619, 54);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btnModificar
            // 
            this.btnModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.btnModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModificar.BackgroundImage")));
            this.btnModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnModificar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificar.FlatAppearance.BorderSize = 0;
            this.btnModificar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnModificar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificar.Location = new System.Drawing.Point(3, 3);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(60, 46);
            this.btnModificar.TabIndex = 4;
            this.btnModificar.UseVisualStyleBackColor = false;
            this.btnModificar.Visible = false;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.btnEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEliminar.BackgroundImage")));
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(50)))), ((int)(((byte)(62)))));
            this.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(50)))), ((int)(((byte)(62)))));
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Location = new System.Drawing.Point(69, 3);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(60, 46);
            this.btnEliminar.TabIndex = 3;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(10, 36);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(242, 72);
            this.txtTotal.TabIndex = 4;
            // 
            // txtVuelto
            // 
            this.txtVuelto.Enabled = false;
            this.txtVuelto.Font = new System.Drawing.Font("Verdana", 39.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVuelto.Location = new System.Drawing.Point(10, 247);
            this.txtVuelto.Name = "txtVuelto";
            this.txtVuelto.Size = new System.Drawing.Size(242, 72);
            this.txtVuelto.TabIndex = 8;
            // 
            // gbTotal
            // 
            this.gbTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTotal.BackColor = System.Drawing.SystemColors.Control;
            this.gbTotal.Controls.Add(this.txtPagaCon);
            this.gbTotal.Controls.Add(this.lblPagaCon);
            this.gbTotal.Controls.Add(this.lblVuelto);
            this.gbTotal.Controls.Add(this.txtVuelto);
            this.gbTotal.Controls.Add(this.lblTotal);
            this.gbTotal.Controls.Add(this.txtTotal);
            this.gbTotal.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTotal.ForeColor = System.Drawing.Color.Blue;
            this.gbTotal.Location = new System.Drawing.Point(630, 242);
            this.gbTotal.Name = "gbTotal";
            this.gbTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbTotal.Size = new System.Drawing.Size(262, 327);
            this.gbTotal.TabIndex = 14;
            this.gbTotal.TabStop = false;
            // 
            // lblPagaCon
            // 
            this.lblPagaCon.AutoSize = true;
            this.lblPagaCon.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPagaCon.ForeColor = System.Drawing.Color.Black;
            this.lblPagaCon.Location = new System.Drawing.Point(8, 120);
            this.lblPagaCon.Name = "lblPagaCon";
            this.lblPagaCon.Size = new System.Drawing.Size(103, 20);
            this.lblPagaCon.TabIndex = 10;
            this.lblPagaCon.Text = "Paga con ($)";
            // 
            // lblVuelto
            // 
            this.lblVuelto.AutoSize = true;
            this.lblVuelto.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVuelto.ForeColor = System.Drawing.Color.Black;
            this.lblVuelto.Location = new System.Drawing.Point(8, 226);
            this.lblVuelto.Name = "lblVuelto";
            this.lblVuelto.Size = new System.Drawing.Size(99, 20);
            this.lblVuelto.TabIndex = 9;
            this.lblVuelto.Text = "Su Vuelto ($)";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(8, 15);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(64, 20);
            this.lblTotal.TabIndex = 5;
            this.lblTotal.Text = "Total ($)";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(145, 0);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(140, 57);
            this.btnCancelar.TabIndex = 19;
            this.btnCancelar.Text = "  Cancelar";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConfirmar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.btnConfirmar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnConfirmar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfirmar.FlatAppearance.BorderSize = 0;
            this.btnConfirmar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnConfirmar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.ForeColor = System.Drawing.Color.White;
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.Location = new System.Drawing.Point(0, 0);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(140, 57);
            this.btnConfirmar.TabIndex = 18;
            this.btnConfirmar.Text = "  Confirmar";
            this.btnConfirmar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfirmar.UseVisualStyleBackColor = false;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // lblOperacionARealizar
            // 
            this.lblOperacionARealizar.AutoSize = true;
            this.lblOperacionARealizar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperacionARealizar.ForeColor = System.Drawing.Color.Black;
            this.lblOperacionARealizar.Location = new System.Drawing.Point(7, 19);
            this.lblOperacionARealizar.Name = "lblOperacionARealizar";
            this.lblOperacionARealizar.Size = new System.Drawing.Size(137, 21);
            this.lblOperacionARealizar.TabIndex = 21;
            this.lblOperacionARealizar.Text = "¿Que operación";
            // 
            // rbVenta
            // 
            this.rbVenta.AutoSize = true;
            this.rbVenta.Checked = true;
            this.rbVenta.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVenta.ForeColor = System.Drawing.Color.Black;
            this.rbVenta.Location = new System.Drawing.Point(7, 86);
            this.rbVenta.Name = "rbVenta";
            this.rbVenta.Size = new System.Drawing.Size(77, 25);
            this.rbVenta.TabIndex = 22;
            this.rbVenta.TabStop = true;
            this.rbVenta.Text = "Venta";
            this.rbVenta.UseVisualStyleBackColor = true;
            this.rbVenta.CheckedChanged += new System.EventHandler(this.rbVenta_CheckedChanged);
            // 
            // rbNotaDeCredito
            // 
            this.rbNotaDeCredito.AutoSize = true;
            this.rbNotaDeCredito.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbNotaDeCredito.ForeColor = System.Drawing.Color.Black;
            this.rbNotaDeCredito.Location = new System.Drawing.Point(7, 124);
            this.rbNotaDeCredito.Name = "rbNotaDeCredito";
            this.rbNotaDeCredito.Size = new System.Drawing.Size(156, 25);
            this.rbNotaDeCredito.TabIndex = 23;
            this.rbNotaDeCredito.Text = "Nota de Crédito";
            this.rbNotaDeCredito.UseVisualStyleBackColor = true;
            this.rbNotaDeCredito.Click += new System.EventHandler(this.rbNotaDeCredito_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.lblOperacionARealizar2);
            this.groupBox1.Controls.Add(this.rbNotaDeCredito);
            this.groupBox1.Controls.Add(this.lblOperacionARealizar);
            this.groupBox1.Controls.Add(this.rbVenta);
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(5, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(165, 162);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // lblOperacionARealizar2
            // 
            this.lblOperacionARealizar2.AutoSize = true;
            this.lblOperacionARealizar2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperacionARealizar2.ForeColor = System.Drawing.Color.Black;
            this.lblOperacionARealizar2.Location = new System.Drawing.Point(18, 40);
            this.lblOperacionARealizar2.Name = "lblOperacionARealizar2";
            this.lblOperacionARealizar2.Size = new System.Drawing.Size(126, 21);
            this.lblOperacionARealizar2.TabIndex = 24;
            this.lblOperacionARealizar2.Text = "desea realizar?";
            // 
            // barraDeAcciones
            // 
            this.barraDeAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.barraDeAcciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.barraDeAcciones.Controls.Add(this.btnConfirmar);
            this.barraDeAcciones.Controls.Add(this.btnCancelar);
            this.barraDeAcciones.Controls.Add(this.btnAgregar);
            this.barraDeAcciones.Location = new System.Drawing.Point(5, 11);
            this.barraDeAcciones.Name = "barraDeAcciones";
            this.barraDeAcciones.Size = new System.Drawing.Size(884, 57);
            this.barraDeAcciones.TabIndex = 21;
            // 
            // ucBuscarProducto1
            // 
            this.ucBuscarProducto1.AutoSize = true;
            this.ucBuscarProducto1.BackColor = System.Drawing.SystemColors.Control;
            this.ucBuscarProducto1.Location = new System.Drawing.Point(179, 86);
            this.ucBuscarProducto1.Name = "ucBuscarProducto1";
            this.ucBuscarProducto1.Size = new System.Drawing.Size(701, 72);
            this.ucBuscarProducto1.TabIndex = 17;
            // 
            // txtPagaCon
            // 
            this.txtPagaCon.Font = new System.Drawing.Font("Verdana", 39.75F);
            this.txtPagaCon.Location = new System.Drawing.Point(10, 142);
            this.txtPagaCon.Name = "txtPagaCon";
            this.txtPagaCon.Size = new System.Drawing.Size(242, 72);
            this.txtPagaCon.TabIndex = 20;
            this.txtPagaCon.TextChanged += new System.EventHandler(this.txtPagaCon_TextChanged);
            // 
            // txtCantidadPesos
            // 
            this.txtCantidadPesos.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidadPesos.Location = new System.Drawing.Point(448, 126);
            this.txtCantidadPesos.Name = "txtCantidadPesos";
            this.txtCantidadPesos.Size = new System.Drawing.Size(80, 27);
            this.txtCantidadPesos.TabIndex = 31;
            this.txtCantidadPesos.TextChanged += new System.EventHandler(this.txtCantidadPesos_TextChanged);
            this.txtCantidadPesos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCantidadPesos_KeyDown);
            // 
            // txtPrecioVentaProducto
            // 
            this.txtPrecioVentaProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioVentaProducto.Location = new System.Drawing.Point(94, 126);
            this.txtPrecioVentaProducto.Name = "txtPrecioVentaProducto";
            this.txtPrecioVentaProducto.Size = new System.Drawing.Size(80, 27);
            this.txtPrecioVentaProducto.TabIndex = 30;
            this.txtPrecioVentaProducto.TextChanged += new System.EventHandler(this.txtPrecioVentaProducto_TextChanged);
            this.txtPrecioVentaProducto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrecioVentaProducto_KeyDown);
            // 
            // Product
            // 
            this.Product.DataPropertyName = "Producto";
            this.Product.FillWeight = 215F;
            this.Product.HeaderText = "Producto";
            this.Product.Name = "Product";
            this.Product.ReadOnly = true;
            this.Product.ToolTipText = "Nombre del producto";
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "Cantidad";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.Cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.Cantidad.FillWeight = 40F;
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Cantidad.ToolTipText = "cantidad del producto.";
            // 
            // PrecioVenta
            // 
            this.PrecioVenta.DataPropertyName = "PrecioVentaUnitario";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.PrecioVenta.DefaultCellStyle = dataGridViewCellStyle3;
            this.PrecioVenta.FillWeight = 58F;
            this.PrecioVenta.HeaderText = "Precio Venta";
            this.PrecioVenta.Name = "PrecioVenta";
            this.PrecioVenta.ReadOnly = true;
            this.PrecioVenta.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PrecioVenta.ToolTipText = "Precio de venta del producto.";
            // 
            // Subtotal
            // 
            this.Subtotal.DataPropertyName = "Subtotal";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.Control;
            this.Subtotal.DefaultCellStyle = dataGridViewCellStyle4;
            this.Subtotal.FillWeight = 42F;
            this.Subtotal.HeaderText = "Subtotal";
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.ReadOnly = true;
            this.Subtotal.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Subtotal.ToolTipText = "Subtotal del producto.";
            // 
            // PrecioVentaAux
            // 
            this.PrecioVentaAux.DataPropertyName = "PrecioVentaAux";
            this.PrecioVentaAux.FillWeight = 30F;
            this.PrecioVentaAux.HeaderText = "PrecioVentaAux";
            this.PrecioVentaAux.Name = "PrecioVentaAux";
            this.PrecioVentaAux.ReadOnly = true;
            this.PrecioVentaAux.Visible = false;
            // 
            // IdProducto
            // 
            this.IdProducto.DataPropertyName = "IdProducto";
            this.IdProducto.FillWeight = 30F;
            this.IdProducto.HeaderText = "IdProducto";
            this.IdProducto.Name = "IdProducto";
            this.IdProducto.ReadOnly = true;
            this.IdProducto.Visible = false;
            // 
            // FrmRegistrarVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(900, 578);
            this.Controls.Add(this.barraDeAcciones);
            this.Controls.Add(this.ucBuscarProducto1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbTotal);
            this.Controls.Add(this.gbDetalleVenta);
            this.Controls.Add(this.gbProductos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "FrmRegistrarVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar Venta";
            this.Load += new System.EventHandler(this.FrmRegistrarVenta_Load);
            this.gbProductos.ResumeLayout(false);
            this.gbProductos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDetalleVenta)).EndInit();
            this.gbDetalleVenta.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.gbTotal.ResumeLayout(false);
            this.gbTotal.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.barraDeAcciones.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbProductos;
        private System.Windows.Forms.DataGridView gvDetalleVenta;
        private System.Windows.Forms.GroupBox gbDetalleVenta;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtVuelto;
        private System.Windows.Forms.GroupBox gbTotal;
        private System.Windows.Forms.Label lblPagaCon;
        private System.Windows.Forms.Label lblVuelto;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.NumericUpDown txtCantidad;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton rbNotaDeCredito;
        private System.Windows.Forms.RadioButton rbVenta;
        private System.Windows.Forms.Label lblOperacionARealizar;
        private System.Windows.Forms.CheckBox cbActualizarPrecioVenta;
        private System.Windows.Forms.CheckBox cbCantidadPesos;
        private System.Windows.Forms.GroupBox groupBox1;
        private MLCustomControls.DecimalTextBox txtPrecioVentaProducto;
        private MLCustomControls.DecimalTextBox txtCantidadPesos;
        private MLCustomControls.DecimalTextBox txtPagaCon;
        private MLCustomControls.UCBuscarProducto ucBuscarProducto1;
        private System.Windows.Forms.TextBox txtNombreProducto;
        private System.Windows.Forms.Label lblNombreProducto;
        private System.Windows.Forms.Label lblPrecioVentaProducto;
        private System.Windows.Forms.TextBox txtIdProducto;
        private System.Windows.Forms.Label lblIdProducto;
        private System.Windows.Forms.Panel barraDeAcciones;
        private System.Windows.Forms.Label lblOperacionARealizar2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioVentaAux;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdProducto;
    }
}