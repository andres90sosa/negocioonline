﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MLContract;
using MLService;

namespace MLDesktop
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            IServicioProducto servicioProducto = new ServicioProducto();
            var prod = servicioProducto.Buscar(p => p.Id == 1);
            Application.Run(new FrmPrincipal());
        }
    }
}
