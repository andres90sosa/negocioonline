﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace MLDesktop
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Maneja el evento cuando se hace click en el botón <see cref="btnClose"/>. Cierra el formulario.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Maneja el evento cuando se hace click en el botón <see cref="btnCollapse"/>. Despliega o repliega el menú de opciones.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnCollapse_Click(object sender, EventArgs e)
        {
            try
            {
                if (menuPanel.Visible)
                    menuPanel.Visible = false;
                else
                    menuPanel.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al presionar el botón de despliegue del menú", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnRestore"/>. Muestra el formulario al estado Normal.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnRestore_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnRestore.Visible = false;
            btnMaximize.Visible = true;
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnMaximize"/>. Maximiza el formulario.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnMaximize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnMaximize.Visible = false;
            btnRestore.Visible = true;
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnMinimize"/>. Minimiza el formulario.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        /// <summary>
        /// Abre un formulario hijo en el contenedor del formulario principal.
        /// </summary>
        /// <param name="childrenForm">The children form.</param>
        private void OpenChildrenForm(Form childrenForm)
        {
            if (contentPanel.Controls.Count > 0)
                contentPanel.Controls.RemoveAt(0);

            childrenForm.TopLevel = false;
            childrenForm.Dock = DockStyle.Fill;
            contentPanel.Controls.Add(childrenForm);
            contentPanel.Tag = childrenForm;
            childrenForm.Show();
        }

        /// <summary>
        /// Maneja el evento MouseDown del control <see cref="headerPanel"/>. Permite desplazar el formulario por la pantalla.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void headerPanel_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                ReleaseCapture();
                SendMessage(this.Handle, 0x112, 0xf012, 0);

                if (WindowState == FormWindowState.Normal && btnRestore.Visible)
                {
                    btnRestore.Visible = false;
                    btnMaximize.Visible = true;
                }
                    
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al desplazar formulario", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnVentas"/>. Llama al método <see cref="OpenChildrenForm(Form)"/>
        /// para abrir el formulario <see cref="FrmRegistrarVenta"/> en el contenedor del formulario principal.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnVentas_Click(object sender, EventArgs e)
        {
            OpenChildrenForm(new FrmRegistrarVenta());
            lblTituloForm.Text = "Registrar Venta";
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnProductos"/>. Llama al método <see cref="OpenChildrenForm(Form)"/>
        /// para abrir el formulario <see cref="FrmAdministrarProductos"/> en el contenedor del formulario principal.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnProductos_Click(object sender, EventArgs e)
        {
            OpenChildrenForm(new FrmAdministrarProductos());
            lblTituloForm.Text = "Administrar Productos";
        }

        /// <summary>
        /// Maneja el evento cuando se hace click sobre el botón <see cref="btnReportes"/>. Llama al método <see cref="OpenChildrenForm(Form)"/>
        /// para abrir el formulario <see cref="FrmListadoVentasDiarias"/> en el contenedor del formulario principal.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnReportes_Click(object sender, EventArgs e)
        {
            OpenChildrenForm(new FrmListadoVentasDiarias());
            lblTituloForm.Text = "Listado de Ventas Diarias";
        }
    }
}
