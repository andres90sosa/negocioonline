﻿namespace MLDesktop
{
    partial class FrmAdministrarProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAdministrarProductos));
            this.gbBuscarProducto = new System.Windows.Forms.GroupBox();
            this.gbDatosProducto = new System.Windows.Forms.GroupBox();
            this.txtPorcentajeGanancia = new MLCustomControls.DecimalTextBox();
            this.txtPrecioVenta = new MLCustomControls.DecimalTextBox();
            this.txtPrecioCosto = new MLCustomControls.DecimalTextBox();
            this.cbTipoProducto3 = new System.Windows.Forms.ComboBox();
            this.cbTipoProducto2 = new System.Windows.Forms.ComboBox();
            this.txtCodigoBarra = new System.Windows.Forms.TextBox();
            this.lblCodigoBarra = new System.Windows.Forms.Label();
            this.lblPorcentajeGanancia = new System.Windows.Forms.Label();
            this.lblPrecioVenta = new System.Windows.Forms.Label();
            this.lblPrecioCosto = new System.Windows.Forms.Label();
            this.cbMarcaProducto = new System.Windows.Forms.ComboBox();
            this.lblMarcaProducto = new System.Windows.Forms.Label();
            this.cbTipoProducto = new System.Windows.Forms.ComboBox();
            this.lblTipoProducto = new System.Windows.Forms.Label();
            this.txtNombreProducto = new System.Windows.Forms.TextBox();
            this.lblNombreProducto = new System.Windows.Forms.Label();
            this.txtIdProducto = new System.Windows.Forms.TextBox();
            this.lblIdProducto = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.barraDeAcciones = new System.Windows.Forms.Panel();
            this.ucBuscarProducto2 = new MLCustomControls.UCBuscarProducto();
            this.gbDatosProducto.SuspendLayout();
            this.barraDeAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbBuscarProducto
            // 
            this.gbBuscarProducto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBuscarProducto.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbBuscarProducto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbBuscarProducto.ForeColor = System.Drawing.Color.Black;
            this.gbBuscarProducto.Location = new System.Drawing.Point(12, 72);
            this.gbBuscarProducto.Name = "gbBuscarProducto";
            this.gbBuscarProducto.Size = new System.Drawing.Size(872, 100);
            this.gbBuscarProducto.TabIndex = 0;
            this.gbBuscarProducto.TabStop = false;
            this.gbBuscarProducto.Text = "Buscar Producto";
            // 
            // gbDatosProducto
            // 
            this.gbDatosProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDatosProducto.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbDatosProducto.Controls.Add(this.txtPorcentajeGanancia);
            this.gbDatosProducto.Controls.Add(this.txtPrecioVenta);
            this.gbDatosProducto.Controls.Add(this.txtPrecioCosto);
            this.gbDatosProducto.Controls.Add(this.cbTipoProducto3);
            this.gbDatosProducto.Controls.Add(this.cbTipoProducto2);
            this.gbDatosProducto.Controls.Add(this.txtCodigoBarra);
            this.gbDatosProducto.Controls.Add(this.lblCodigoBarra);
            this.gbDatosProducto.Controls.Add(this.lblPorcentajeGanancia);
            this.gbDatosProducto.Controls.Add(this.lblPrecioVenta);
            this.gbDatosProducto.Controls.Add(this.lblPrecioCosto);
            this.gbDatosProducto.Controls.Add(this.cbMarcaProducto);
            this.gbDatosProducto.Controls.Add(this.lblMarcaProducto);
            this.gbDatosProducto.Controls.Add(this.cbTipoProducto);
            this.gbDatosProducto.Controls.Add(this.lblTipoProducto);
            this.gbDatosProducto.Controls.Add(this.txtNombreProducto);
            this.gbDatosProducto.Controls.Add(this.lblNombreProducto);
            this.gbDatosProducto.Controls.Add(this.txtIdProducto);
            this.gbDatosProducto.Controls.Add(this.lblIdProducto);
            this.gbDatosProducto.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbDatosProducto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDatosProducto.ForeColor = System.Drawing.Color.Black;
            this.gbDatosProducto.Location = new System.Drawing.Point(12, 178);
            this.gbDatosProducto.Name = "gbDatosProducto";
            this.gbDatosProducto.Size = new System.Drawing.Size(872, 287);
            this.gbDatosProducto.TabIndex = 1;
            this.gbDatosProducto.TabStop = false;
            this.gbDatosProducto.Text = "Datos del Producto";
            // 
            // txtPorcentajeGanancia
            // 
            this.txtPorcentajeGanancia.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorcentajeGanancia.Location = new System.Drawing.Point(564, 189);
            this.txtPorcentajeGanancia.Name = "txtPorcentajeGanancia";
            this.txtPorcentajeGanancia.Size = new System.Drawing.Size(95, 27);
            this.txtPorcentajeGanancia.TabIndex = 16;
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioVenta.Location = new System.Drawing.Point(181, 240);
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.Size = new System.Drawing.Size(95, 27);
            this.txtPrecioVenta.TabIndex = 18;
            // 
            // txtPrecioCosto
            // 
            this.txtPrecioCosto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioCosto.Location = new System.Drawing.Point(181, 189);
            this.txtPrecioCosto.Name = "txtPrecioCosto";
            this.txtPrecioCosto.Size = new System.Drawing.Size(95, 27);
            this.txtPrecioCosto.TabIndex = 14;
            // 
            // cbTipoProducto3
            // 
            this.cbTipoProducto3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoProducto3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoProducto3.FormattingEnabled = true;
            this.cbTipoProducto3.Items.AddRange(new object[] {
            ""});
            this.cbTipoProducto3.Location = new System.Drawing.Point(644, 85);
            this.cbTipoProducto3.MaxLength = 50;
            this.cbTipoProducto3.Name = "cbTipoProducto3";
            this.cbTipoProducto3.Size = new System.Drawing.Size(219, 29);
            this.cbTipoProducto3.TabIndex = 10;
            this.cbTipoProducto3.Visible = false;
            // 
            // cbTipoProducto2
            // 
            this.cbTipoProducto2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoProducto2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoProducto2.FormattingEnabled = true;
            this.cbTipoProducto2.Items.AddRange(new object[] {
            ""});
            this.cbTipoProducto2.Location = new System.Drawing.Point(419, 85);
            this.cbTipoProducto2.MaxLength = 50;
            this.cbTipoProducto2.Name = "cbTipoProducto2";
            this.cbTipoProducto2.Size = new System.Drawing.Size(219, 29);
            this.cbTipoProducto2.TabIndex = 9;
            this.cbTipoProducto2.Visible = false;
            this.cbTipoProducto2.SelectedIndexChanged += new System.EventHandler(this.cbTipoProducto2_SelectedIndexChanged);
            // 
            // txtCodigoBarra
            // 
            this.txtCodigoBarra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoBarra.Location = new System.Drawing.Point(564, 240);
            this.txtCodigoBarra.MaxLength = 50;
            this.txtCodigoBarra.Name = "txtCodigoBarra";
            this.txtCodigoBarra.Size = new System.Drawing.Size(299, 27);
            this.txtCodigoBarra.TabIndex = 20;
            // 
            // lblCodigoBarra
            // 
            this.lblCodigoBarra.AutoSize = true;
            this.lblCodigoBarra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoBarra.ForeColor = System.Drawing.Color.Black;
            this.lblCodigoBarra.Location = new System.Drawing.Point(375, 243);
            this.lblCodigoBarra.Name = "lblCodigoBarra";
            this.lblCodigoBarra.Size = new System.Drawing.Size(142, 21);
            this.lblCodigoBarra.TabIndex = 19;
            this.lblCodigoBarra.Text = "Código de Barra:";
            // 
            // lblPorcentajeGanancia
            // 
            this.lblPorcentajeGanancia.AutoSize = true;
            this.lblPorcentajeGanancia.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorcentajeGanancia.ForeColor = System.Drawing.Color.Black;
            this.lblPorcentajeGanancia.Location = new System.Drawing.Point(375, 192);
            this.lblPorcentajeGanancia.Name = "lblPorcentajeGanancia";
            this.lblPorcentajeGanancia.Size = new System.Drawing.Size(183, 21);
            this.lblPorcentajeGanancia.TabIndex = 15;
            this.lblPorcentajeGanancia.Text = "Porcentaje Ganancia:";
            // 
            // lblPrecioVenta
            // 
            this.lblPrecioVenta.AutoSize = true;
            this.lblPrecioVenta.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioVenta.ForeColor = System.Drawing.Color.Black;
            this.lblPrecioVenta.Location = new System.Drawing.Point(8, 243);
            this.lblPrecioVenta.Name = "lblPrecioVenta";
            this.lblPrecioVenta.Size = new System.Drawing.Size(139, 21);
            this.lblPrecioVenta.TabIndex = 17;
            this.lblPrecioVenta.Text = "Precio de Venta:";
            // 
            // lblPrecioCosto
            // 
            this.lblPrecioCosto.AutoSize = true;
            this.lblPrecioCosto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioCosto.ForeColor = System.Drawing.Color.Black;
            this.lblPrecioCosto.Location = new System.Drawing.Point(8, 192);
            this.lblPrecioCosto.Name = "lblPrecioCosto";
            this.lblPrecioCosto.Size = new System.Drawing.Size(136, 21);
            this.lblPrecioCosto.TabIndex = 13;
            this.lblPrecioCosto.Text = "Precio de Costo:";
            // 
            // cbMarcaProducto
            // 
            this.cbMarcaProducto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMarcaProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMarcaProducto.FormattingEnabled = true;
            this.cbMarcaProducto.Items.AddRange(new object[] {
            ""});
            this.cbMarcaProducto.Location = new System.Drawing.Point(181, 135);
            this.cbMarcaProducto.MaxLength = 50;
            this.cbMarcaProducto.Name = "cbMarcaProducto";
            this.cbMarcaProducto.Size = new System.Drawing.Size(232, 29);
            this.cbMarcaProducto.TabIndex = 12;
            // 
            // lblMarcaProducto
            // 
            this.lblMarcaProducto.AutoSize = true;
            this.lblMarcaProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarcaProducto.ForeColor = System.Drawing.Color.Black;
            this.lblMarcaProducto.Location = new System.Drawing.Point(8, 140);
            this.lblMarcaProducto.Name = "lblMarcaProducto";
            this.lblMarcaProducto.Size = new System.Drawing.Size(167, 21);
            this.lblMarcaProducto.TabIndex = 11;
            this.lblMarcaProducto.Text = "Marca de Producto:";
            // 
            // cbTipoProducto
            // 
            this.cbTipoProducto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoProducto.FormattingEnabled = true;
            this.cbTipoProducto.Items.AddRange(new object[] {
            ""});
            this.cbTipoProducto.Location = new System.Drawing.Point(181, 85);
            this.cbTipoProducto.MaxLength = 50;
            this.cbTipoProducto.Name = "cbTipoProducto";
            this.cbTipoProducto.Size = new System.Drawing.Size(232, 29);
            this.cbTipoProducto.TabIndex = 8;
            this.cbTipoProducto.SelectedIndexChanged += new System.EventHandler(this.cbTipoProducto_SelectedIndexChanged);
            // 
            // lblTipoProducto
            // 
            this.lblTipoProducto.AutoSize = true;
            this.lblTipoProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoProducto.ForeColor = System.Drawing.Color.Black;
            this.lblTipoProducto.Location = new System.Drawing.Point(7, 88);
            this.lblTipoProducto.Name = "lblTipoProducto";
            this.lblTipoProducto.Size = new System.Drawing.Size(147, 21);
            this.lblTipoProducto.TabIndex = 7;
            this.lblTipoProducto.Text = "Tipo de Producto:";
            // 
            // txtNombreProducto
            // 
            this.txtNombreProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreProducto.Location = new System.Drawing.Point(181, 34);
            this.txtNombreProducto.MaxLength = 50;
            this.txtNombreProducto.Name = "txtNombreProducto";
            this.txtNombreProducto.Size = new System.Drawing.Size(514, 27);
            this.txtNombreProducto.TabIndex = 4;
            // 
            // lblNombreProducto
            // 
            this.lblNombreProducto.AutoSize = true;
            this.lblNombreProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreProducto.ForeColor = System.Drawing.Color.Black;
            this.lblNombreProducto.Location = new System.Drawing.Point(8, 37);
            this.lblNombreProducto.Name = "lblNombreProducto";
            this.lblNombreProducto.Size = new System.Drawing.Size(77, 21);
            this.lblNombreProducto.TabIndex = 3;
            this.lblNombreProducto.Text = "Nombre:";
            // 
            // txtIdProducto
            // 
            this.txtIdProducto.Enabled = false;
            this.txtIdProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdProducto.Location = new System.Drawing.Point(764, 37);
            this.txtIdProducto.MaxLength = 7;
            this.txtIdProducto.Name = "txtIdProducto";
            this.txtIdProducto.Size = new System.Drawing.Size(99, 27);
            this.txtIdProducto.TabIndex = 6;
            // 
            // lblIdProducto
            // 
            this.lblIdProducto.AutoSize = true;
            this.lblIdProducto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdProducto.ForeColor = System.Drawing.Color.Black;
            this.lblIdProducto.Location = new System.Drawing.Point(728, 40);
            this.lblIdProducto.Name = "lblIdProducto";
            this.lblIdProducto.Size = new System.Drawing.Size(30, 21);
            this.lblIdProducto.TabIndex = 5;
            this.lblIdProducto.Text = "Id:";
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(53)))), ((int)(((byte)(69)))));
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(50)))), ((int)(((byte)(62)))));
            this.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(50)))), ((int)(((byte)(62)))));
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.Location = new System.Drawing.Point(145, 0);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(140, 57);
            this.btnEliminar.TabIndex = 22;
            this.btnEliminar.Text = "  Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(0, 0);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(140, 57);
            this.btnGuardar.TabIndex = 21;
            this.btnGuardar.Text = "  Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(130)))), ((int)(((byte)(72)))));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(290, 0);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(140, 57);
            this.btnCancelar.TabIndex = 23;
            this.btnCancelar.Text = "  Cancelar";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // barraDeAcciones
            // 
            this.barraDeAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.barraDeAcciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(53)))), ((int)(((byte)(65)))));
            this.barraDeAcciones.Controls.Add(this.btnGuardar);
            this.barraDeAcciones.Controls.Add(this.btnEliminar);
            this.barraDeAcciones.Controls.Add(this.btnCancelar);
            this.barraDeAcciones.Location = new System.Drawing.Point(12, 12);
            this.barraDeAcciones.Name = "barraDeAcciones";
            this.barraDeAcciones.Size = new System.Drawing.Size(872, 57);
            this.barraDeAcciones.TabIndex = 2;
            // 
            // ucBuscarProducto2
            // 
            this.ucBuscarProducto2.AutoSize = true;
            this.ucBuscarProducto2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ucBuscarProducto2.Location = new System.Drawing.Point(14, 86);
            this.ucBuscarProducto2.Name = "ucBuscarProducto2";
            this.ucBuscarProducto2.Size = new System.Drawing.Size(830, 74);
            this.ucBuscarProducto2.TabIndex = 1;
            // 
            // FrmAdministrarProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(896, 473);
            this.Controls.Add(this.barraDeAcciones);
            this.Controls.Add(this.ucBuscarProducto2);
            this.Controls.Add(this.gbBuscarProducto);
            this.Controls.Add(this.gbDatosProducto);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "FrmAdministrarProductos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Productos";
            this.Load += new System.EventHandler(this.FrmAdministrarProducto_Load);
            this.gbDatosProducto.ResumeLayout(false);
            this.gbDatosProducto.PerformLayout();
            this.barraDeAcciones.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBuscarProducto;
        private System.Windows.Forms.GroupBox gbDatosProducto;
        private System.Windows.Forms.ComboBox cbTipoProducto;
        private System.Windows.Forms.Label lblTipoProducto;
        private System.Windows.Forms.TextBox txtNombreProducto;
        private System.Windows.Forms.Label lblNombreProducto;
        private System.Windows.Forms.TextBox txtIdProducto;
        private System.Windows.Forms.Label lblIdProducto;
        private System.Windows.Forms.Label lblPorcentajeGanancia;
        private System.Windows.Forms.Label lblPrecioVenta;
        private System.Windows.Forms.Label lblPrecioCosto;
        private System.Windows.Forms.ComboBox cbMarcaProducto;
        private System.Windows.Forms.Label lblMarcaProducto;
        private System.Windows.Forms.TextBox txtCodigoBarra;
        private System.Windows.Forms.Label lblCodigoBarra;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.ComboBox cbTipoProducto3;
        private System.Windows.Forms.ComboBox cbTipoProducto2;
        private MLCustomControls.DecimalTextBox txtPrecioCosto;
        private MLCustomControls.DecimalTextBox txtPrecioVenta;
        private MLCustomControls.DecimalTextBox txtPorcentajeGanancia;
        private MLCustomControls.UCBuscarProducto ucBuscarProducto2;
        private System.Windows.Forms.Panel barraDeAcciones;
    }
}