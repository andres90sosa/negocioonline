﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MLContract;
using MLService;
using MLDomain;
using MLReporting;

namespace MLDesktop
{
    public partial class FrmListadoVentasDiarias : Form
    {
        public FrmListadoVentasDiarias()
        {
            InitializeComponent();
        }

        private void FrmListadoVentasDiarias_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Maneja el evento cuando se hace click en el botón <see cref="btnGenerar"/>.
        /// Valida la generación del listado de ventas diarias e instancia a la clase <see cref="ListadoVentasDiarias"/> para generar el reporte.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="EventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnGenerar_Click(object sender, EventArgs e)
        {
            DateTime fecha;
            try
            {
                fecha = Convert.ToDateTime(dtpFechaVenta.Text);
                
                if (DateTime.Compare(fecha,DateTime.Now) <= 0)
                {
                    ListadoVentasDiarias listado = new ListadoVentasDiarias(fecha, this.reportViewer1);
                    listado.GenerarInforme();

                    if (listado.CountMovimientos <= 0)
                        MessageBox.Show("No existen ventas para la fecha ingresada.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                }
                else
                    MessageBox.Show("La fecha ingresada es superior a la fecha actual. Ingrese una fecha anterior o igual a la fecha actual.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al generar reporte", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
