﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MLDomain;
using MLService;
using MLContract;
using MaterialSkin;

namespace MLDesktop
{
    public partial class FrmAdministrarProductos : Form
    {
        public FrmAdministrarProductos()
        {
            InitializeComponent();
        }

        #region Eventos

        /// <summary>
        /// Maneja el evento cuando se carga el formulario <see cref="FrmAdministrarProductos"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void FrmAdministrarProducto_Load(object sender, EventArgs e)
        {
            try
            {
                CargarMarcasProducto();
                CargarTiposProducto(cbTipoProducto, 1, null);
                HabilitarBoton(btnEliminar, false);

                //Se suscribe al evento EventClickBtnBuscar del user control UCBuscarProducto
                ucBuscarProducto2.EventClickBtnBuscar += new MLCustomControls.UCBuscarProducto.MyEventClick(CargarDatosProducto);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error cargar formulario", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace click en el botón <see cref="btnGuardar"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarProducto();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al guardar el producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace click en el botón <see cref="btnCancelar"/>
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Esta seguro que desea cancelar la operación?", "Cancelar ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    InicializarIU();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al Cancelar operación ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se selecciona un item del combo <see cref="cbTipoProducto"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void cbTipoProducto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbTipoProducto.SelectedIndex != 0 && cbTipoProducto.SelectedIndex != -1)
                    CargarTiposProducto(cbTipoProducto2, 2, (int)cbTipoProducto.SelectedValue);
                else
                    InicializarCombosSubtiposProducto();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al cargar subtipos de producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se selecciona un item del combo <see cref="cbTipoProducto2"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void cbTipoProducto2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbTipoProducto2.SelectedIndex != 0 && cbTipoProducto2.SelectedIndex != -1)
                    CargarTiposProducto(cbTipoProducto3, 3, (int)cbTipoProducto2.SelectedValue);
                else
                {
                    cbTipoProducto3.Visible = false;
                    cbTipoProducto3.DataSource = null;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error al cargar subtipos de producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        /// <summary>
        /// Maneja el evento cuando se hace click en el botón <see cref="btnEliminar"/>.
        /// </summary>
        /// <param name="sender">Control que origina el evento.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> Instancia que contiene los datos del evento.</param>
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Esta seguro que desea eliminar el producto?", "Eliminar ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    IServicioProducto servicioProducto = new ServicioProducto();
                    servicioProducto.Eliminar(int.Parse(txtIdProducto.Text));
                    InicializarIU();
                    MessageBox.Show("El producto se eliminó correctamente!!!", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al eliminar producto", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        #endregion

        #region MetodosDelFormulario

        /// <summary>
        /// Habilita o deshabilita un botón.
        /// </summary>
        /// <param name="btn">botón que se va a habilitar o deshabilitar.</param>
        /// <param name="habilitar">Si se setea en <c>true</c> se habilita el botón, si se setea en <c>true</c> se deshabilita el botón.</param>
        /// <exception cref="ApplicationException"></exception>
        private void HabilitarBoton(Button btn, bool habilitar)
        {
            try
            {
                if (habilitar)
                {
                    btn.Enabled = true;
                    btn.BackColor = Color.FromArgb(220, 53, 69);
                }
                else
                {
                    btn.Enabled = false;
                    btn.BackColor = Color.LightGray;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Carga las marcas de producto en el combo <see cref="cbMarcaProducto"/>.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void CargarMarcasProducto()
        {
            List<MarcaProducto> marcasProducto = null;
            try
            {
                IServicio<MarcaProducto> servMarcaProd = new Servicio<MarcaProducto>();
                marcasProducto = new List<MarcaProducto>();
                marcasProducto.Add(new MarcaProducto() { Nombre = "Elegir una opción", Id = 0 });
                marcasProducto.AddRange(servMarcaProd.ObtenerTodo(false).ToList());
                cbMarcaProducto.DataSource = marcasProducto;
                cbMarcaProducto.DisplayMember = "Nombre";
                cbMarcaProducto.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Carga los tipos de producto de un determinado nivel de jerarquía.
        /// </summary>
        /// <param name="combo">Combo a cargar con los tipos de producto.</param>
        /// <param name="nivel">Nivel de jerarquía del tipo de producto.</param>
        /// <param name="idTipoProductoPadre">Identificador del tipo de producto padre.</param>
        /// <exception cref="ApplicationException"></exception>
        private void CargarTiposProducto(ComboBox combo, int nivel, int? idTipoProductoPadre)
        {
            List<TipoProducto> tiposProducto = null;
            try
            {
                IServicioTipoProducto servTipoProd = new ServicioTipoProducto();
                tiposProducto = new List<TipoProducto>();
                tiposProducto.Add(new TipoProducto() { Nombre = "Elegir una opción", Id = 0 });
                tiposProducto.AddRange(servTipoProd.ObtenerPorNivel(nivel, idTipoProductoPadre).ToList());
                combo.DataSource = tiposProducto;
                combo.DisplayMember = "Nombre";
                combo.ValueMember = "Id";

                if (combo.Items.Count > 1)
                    combo.Visible = true;
                else
                    combo.Visible = false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Recibe el evento EventClickBtnBuscar del user control UCBuscarProducto.
        /// Se cargan los datos del producto buscado en los campos correspondientes para poder editarlos.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void CargarDatosProducto()
        {
            List<TipoProducto> jerarquiaTiposProducto = null;
            Producto p = null;
            try
            {
                p = ucBuscarProducto2.ProductoSeleccionado;

                if(p != null)
                {
                    InicializarCamposDatosProducto();
                    txtNombreProducto.Text = p.Nombre;
                    txtIdProducto.Text = p.Id.ToString();
                    IServicioTipoProducto servicioTipoProducto = new ServicioTipoProducto();
                    jerarquiaTiposProducto = servicioTipoProducto.ObtenerJerarquiaDeTipos(p.IdTipoProd).ToList();

                    foreach (var tipoProd in jerarquiaTiposProducto)
                    {
                        switch (tipoProd.Nivel)
                        {
                            case 1:
                                cbTipoProducto.SelectedValue = tipoProd.Id;
                                break;
                            case 2:
                                cbTipoProducto2.SelectedValue = tipoProd.Id;
                                break;
                            case 3:
                                cbTipoProducto3.SelectedValue = tipoProd.Id;
                                break;
                        }
                    }

                    if (p.PrecioCosto.HasValue)
                        txtPrecioCosto.Text = p.PrecioCosto.ToString();

                    if (p.PrecioVenta.HasValue)
                        txtPrecioVenta.Text = p.PrecioVenta.ToString();

                    if (p.IdMarcaProd.HasValue)
                        cbMarcaProducto.SelectedValue = p.IdMarcaProd;

                    if (p.PorcentajeGanancia.HasValue)
                        txtPorcentajeGanancia.Text = p.PorcentajeGanancia.ToString();

                    if (!string.IsNullOrEmpty(p.CodigoBarra))
                        txtCodigoBarra.Text = p.CodigoBarra;

                    HabilitarBoton(btnEliminar, true);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Elimina los items de los combos de subtipos de producto y oculta los combos.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void InicializarCombosSubtiposProducto()
        {
            try
            {
                cbTipoProducto2.Visible = false;
                cbTipoProducto3.Visible = false;
                cbTipoProducto2.DataSource = null;
                cbTipoProducto3.DataSource = null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Valida que se ingresen los datos del producto que son obligatorios.
        /// </summary>
        /// <returns>
        /// Retorna <c>true</c> si se ingresaron los datos correctamente y <c>false</c> en caso contrario.
        /// </returns>
        /// <exception cref="ApplicationException"></exception>
        public bool ValidarDatosProducto()
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombreProducto.Text.Trim()))
                {
                    MessageBox.Show("Ingrese el nombre del producto.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    txtNombreProducto.Focus();
                    return false;
                }
                if (cbTipoProducto.SelectedIndex == 0 || cbTipoProducto.SelectedIndex == -1)
                {
                    MessageBox.Show("Seleccione el tipo de producto.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    cbTipoProducto.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        
        /// <summary>
        /// Valida y guarda los datos de un producto.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void GuardarProducto()
        {
            int idProducto = 0;
            string nombre = "";
            int idTipo = 0;
            int? idMarca = null;
            decimal? precioCosto = null;
            decimal? precioVenta = null;
            decimal? porcentajeGanancia = null;
            string codigoBarra = null;
            Producto producto = null;
            try
            {
                if (ValidarDatosProducto())
                {
                    IServicioProducto servicioProducto = new ServicioProducto();
                    nombre = txtNombreProducto.Text.Trim();

                    if (!string.IsNullOrEmpty(txtIdProducto.Text))
                        idProducto = int.Parse(txtIdProducto.Text);
                    else
                    {
                        //Validamos si existe un producto con el mismo nombre que este eliminado para darlo de alta nuevamente.
                        producto = servicioProducto.ObtenerPorNombre(nombre, true);

                        if (producto != null)
                        {
                            if (producto.IsDeleted)
                                idProducto = producto.Id;
                            else
                            {
                                MessageBox.Show("El producto que intenta guardar ya existe. Busque el producto existente para poder editarlo.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                                return;
                            }
                        }
                    }

                    if (cbTipoProducto2.SelectedIndex != 0 && cbTipoProducto2.SelectedIndex != -1)
                        if (cbTipoProducto3.SelectedIndex != 0 && cbTipoProducto3.SelectedIndex != -1)
                            idTipo = (int)cbTipoProducto3.SelectedValue;
                        else
                            idTipo = (int)cbTipoProducto2.SelectedValue;
                    else
                        idTipo = (int)cbTipoProducto.SelectedValue;

                    if (cbMarcaProducto.SelectedIndex != 0 && cbMarcaProducto.SelectedIndex != -1)
                        idMarca = (int)cbMarcaProducto.SelectedValue;

                    if (!string.IsNullOrEmpty(txtPrecioCosto.Text))
                        precioCosto = decimal.Parse(txtPrecioCosto.Text);

                    if (!string.IsNullOrEmpty(txtPrecioVenta.Text))
                        precioVenta = decimal.Parse(txtPrecioVenta.Text);

                    if (!string.IsNullOrEmpty(txtPorcentajeGanancia.Text))
                        porcentajeGanancia = decimal.Parse(txtPorcentajeGanancia.Text);

                    if (!string.IsNullOrEmpty(txtCodigoBarra.Text.Trim()))
                        codigoBarra = txtCodigoBarra.Text.Trim();

                    Producto p = new Producto(idProducto, nombre, idTipo, idMarca, precioCosto, precioVenta, porcentajeGanancia, codigoBarra);
                    servicioProducto.Guardar(p);
                    InicializarIU();
                    MessageBox.Show("El producto se guardó correctamente!!!", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Inicializa la IU.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void InicializarIU()
        {
            try
            {
                InicializarCamposDatosProducto();
                HabilitarBoton(btnEliminar, false);
                ucBuscarProducto2.ClearTextBoxProducto();
                ucBuscarProducto2.FocusTextBoxProducto();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Inicializa los campos de la sección Datos del Producto.
        /// </summary>
        /// <exception cref="ApplicationException"></exception>
        private void InicializarCamposDatosProducto()
        {
            try
            {
                txtNombreProducto.Text = string.Empty;
                txtIdProducto.Text = string.Empty;
                cbTipoProducto.SelectedIndex = 0;
                cbMarcaProducto.SelectedIndex = 0;
                txtPrecioCosto.Text = string.Empty;
                txtPrecioVenta.Text = string.Empty;
                txtPorcentajeGanancia.Text = string.Empty;
                txtCodigoBarra.Text = string.Empty;
                InicializarCombosSubtiposProducto();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        #endregion

    }
}
