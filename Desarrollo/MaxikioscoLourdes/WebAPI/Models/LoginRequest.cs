﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    /// <summary>
    /// Clase encargada de administrar las credenciales del usuario.
    /// </summary>
    public class LoginRequest
    {

        /// <summary>
        /// Obtine o setea el nombre de usuario.
        /// </summary>
        /// <value>
        /// Nombre de usuario.
        /// </value>
        public string Username { get; set; }


        /// <summary>
        /// Obtiene o setea la contraseña del usuario.
        /// </summary>
        /// <value>
        /// Contraseña del usuario.
        /// </value>
        public string Password { get; set; }
    }
}