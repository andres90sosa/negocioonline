﻿using MLDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.ViewModels
{
    public class VMMarcaProducto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int CodNegocio { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public VMMarcaProducto(MarcaProducto marca)
        {
            Id = marca.Id;
            Nombre = marca.Nombre;
            CodNegocio = marca.CodNegocio;
            FechaCreacion = marca.FechaCreacion;
            FechaModificacion = (DateTime)(marca.FechaModificacion == null ? marca.FechaCreacion : marca.FechaModificacion);
        }
    }
}