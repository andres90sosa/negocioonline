﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MLDomain;

namespace WebAPI.Models.ViewModels
{
    /// <summary>
    /// Mapea los datos de un producto que van a ser utilizados en una vista.
    /// </sumary>
    public class VMProducto
    {
        public int IdProd { get; set; }
        public string Nombre { get; set; }
        public string CodBarra { get; set; }
        public string Imagen { get; set; }
        public string TipoProd { get; set; }
        public string MarcaProd { get; set; }
        public decimal? Precioventa { get; set; }
        public decimal? PrecioCosto { get; set; }
        public decimal? PorcentajeGanancia { get; set; }

        public VMProducto(Producto p)
        {
            IdProd = p.Id;
            Nombre = p.Nombre;
            CodBarra = p.CodigoBarra == null ? string.Empty : p.CodigoBarra;
            Imagen = p.Imagen == null ? string.Empty : p.Imagen;
            TipoProd = p.Tipo.Nombre;
            MarcaProd = p.Marca == null ? string.Empty : p.Marca.Nombre;
            Precioventa = p.PrecioVenta == null ? 0 : p.PrecioVenta;
            PrecioCosto = p.PrecioCosto == null ? 0 : p.PrecioCosto;
            PorcentajeGanancia = p.PorcentajeGanancia == null ? 0 : p.PorcentajeGanancia;
        }
    }
}