﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MLDomain;
using MLContract;
using MLService;
using WebAPI.Models.ViewModels;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Controlador encargado de administrar los datos de las marcas de producto de un negocio.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("api/marcas")]
    public class MarcasController : ApiController
    {
        [HttpPost]
        public IHttpActionResult CrearMarcaProducto(MarcaProducto marca)
          {
            IServicio<MarcaProducto> servicio = null;
            try
            {
                if (string.IsNullOrEmpty(marca.Nombre))
                    throw new ApplicationException("El nombre de la marca de producto es requerido.");
                else if (marca.CodNegocio <= 0)
                    throw new ApplicationException("El código de negocio de la marca de producto es requerido.");

                servicio = new Servicio<MarcaProducto>();
                marca.Nombre.Trim();
                marca.FechaCreacion = DateTime.Now;
                marca.IsDeleted = false;
                servicio.Guardar(marca);
                return Ok();
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(ex.Message)
                });
            }
        }

        [HttpGet]
        public IHttpActionResult ObtenerMarcas(int? codNegocio, int? cantidad)
        {
            IServicioMarcaProducto servicioMarcaProducto = null;
            List<VMMarcaProducto> modelMarcas = null;
            List<MarcaProducto> marcas = null;

            if (codNegocio <= 0 || codNegocio == null)
                throw new ApplicationException("El código de negocio del producto es requerido y debe ser mayor a cero.");

            if (cantidad < 0)
                throw new ApplicationException("La cantidad de marcas no es válida");

            try
            {
                servicioMarcaProducto = new ServicioMarcaProducto();
                modelMarcas = new List<VMMarcaProducto>();
                marcas = new List<MarcaProducto>();

                marcas.AddRange(servicioMarcaProducto.ObtenerPorCantidad(codNegocio.Value, cantidad.Value));
                foreach (var marca in marcas)
                    modelMarcas.Add(new VMMarcaProducto(marca));

                return Ok(modelMarcas);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(ex.Message)
                    });
            }
        }

        [HttpPost]
        public IHttpActionResult ActualizarMarca(int id, string nombre)
        {
            IServicioMarcaProducto servicioMarcaProducto;
            MarcaProducto marcaProducto;
            try
            {
                servicioMarcaProducto = new ServicioMarcaProducto();
                marcaProducto = new MarcaProducto(id, nombre);
                bool response = servicioMarcaProducto.Actualizar(marcaProducto);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(ex.Message)
                    });
            }
        }

        [HttpPost]
        public IHttpActionResult BorrarMarca(int marcaId)
        {
            IServicioMarcaProducto servicioMarcaProducto;

            try
            {
                servicioMarcaProducto = new ServicioMarcaProducto();
                return Ok(servicioMarcaProducto.Borrar(marcaId));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(ex.Message)
                    });
            }
        }
    }
}
