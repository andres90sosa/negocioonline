﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Controlador encargado de autenticar los usuarios.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [AllowAnonymous]
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        /// <summary>
        /// Acción para verificar que el controlador responde OK.
        /// </summary>
        /// <returns>
        /// Retorna true si el controlador responde OK.
        /// </returns>
        [HttpGet]
        [Route("echoping")]
        public IHttpActionResult EchoPing()
        {
            return Ok(true);
        }

        /// <summary>
        /// Acción para determinar si hay algún usuario autenticado.
        /// </summary>
        /// <returns>
        /// Retorna una cadena de texto con el nombre del usuario actual y si el mismo esta está autenticado.
        /// </returns>
        [HttpGet]
        [Route("echouser")]
        public IHttpActionResult EchoUser()
        {
            var identity = Thread.CurrentPrincipal.Identity;
            return Ok($" IPrincipal-user: {identity.Name} - IsAuthenticated: {identity.IsAuthenticated}");
        }


        /// <summary>
        /// Acción para autenticar las credenciales del usuario.
        /// </summary>
        /// <param name="login">Credenciales del usuario.</param>
        /// <returns>
        /// Retorna un token en caso que el usuario se autentique correctamente y un error en caso contrario.
        /// </returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(LoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            // Validamos las credenciales correctamente
            bool isCredentialValid = (login.Password == "1nino2luna" && login.Username == "admin");
            if (isCredentialValid)
            {
                var token = TokenGenerator.GenerateTokenJwt(login.Username);
                return Ok(token);
            }
            else
            {
                return Unauthorized();
            }
        }


    }
}
