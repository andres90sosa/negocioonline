﻿using MLContract;
using MLDomain;
using MLService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models.ViewModels;

namespace WebAPI.Controllers
{
    /// <summary>
    /// Controlador encargado de administrar los datos de los productos de un negocio.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("api/productos")]
    public class ProductosController : ApiController
    {
        /// <summary>
        /// Acción que obtiene los productos de un negocio según el nombre o código de barra indicado.
        /// </summary>
        /// <param name="codNegocio">Código de negocio.</param>
        /// <param name="nombre">Nombre del producto.</param>
        /// <param name="codBarra">Código de barra del producto.</param>
        /// <returns>
        /// Retorna una lista de productos para el negocio especificado.
        /// </returns>
        /// <exception cref="ApplicationException">El nombre o código de barra del producto es requerido.</exception>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        public IHttpActionResult ObtenerProductos(int? codNegocio,string nombre, string codBarra)
        {
            List<VMProducto> productos = null;
            List<Producto> modelProductos = null;
            IServicioProducto servicioProducto = null;
            Producto p = null;

            try
            {
                servicioProducto = new ServicioProducto();
                productos = new List<VMProducto>();
                modelProductos = new List<Producto>();

                if (codNegocio <= 0 || codNegocio == null)
                    throw new ApplicationException("El código de negocio del producto es requerido y debe ser mayor a cero.");

                if (!string.IsNullOrEmpty(nombre))
                {
                    modelProductos.AddRange(servicioProducto.ObtenerPorNombreAproximado(codNegocio.Value, nombre));
                    foreach (var prod in modelProductos)
                        productos.Add(new VMProducto(prod));
                }
                else if (!string.IsNullOrEmpty(codBarra))
                {
                    p = servicioProducto.ObtenerPorCodigoDeBarra(codNegocio.Value, codBarra);
                    if(p != null)
                        productos.Add(new VMProducto(p));
                }
                else
                    throw new ApplicationException("El nombre o código de barra del producto es requerido.");

                return Ok(productos);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(ex.Message)
                });
            }
        }

        [HttpGet]
        public IHttpActionResult ObtenerTodo()
        {
            var productos = "producto-1";
            return Ok(productos);
        }

    }
}
