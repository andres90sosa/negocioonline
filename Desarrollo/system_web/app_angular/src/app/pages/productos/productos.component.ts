import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../services/productos.service';
import { Producto, JSON_Producto } from '../../interfaces/producto.interface';

/* Imports tests */
import { ProductosTestsService } from '../../tests/services/productos-tests.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css', '../../components/share/sidebar-alt/sidebar-alt.component.css']
})

export class ProductosComponent implements OnInit {

  productos:Producto[] = [];
  productosSearch:Producto[] = [];
  termino:string; /* termino de busqueda de productos. */

  constructor( private api: ProductosService,
               private testApi: ProductosTestsService) {
  }

  ngOnInit(): void {
    this.termino = this.api.getStrSearch();
    this.productosSearch = this.testApi.db_buscarProducto(this.termino);
    this.testApi.APIMOCK_getProductos(1, '').subscribe(data => {
      
      this.add_Productos(data);
      console.log(this.productosSearch);

    }, error => {
      console.error(JSON.stringify(error));
    });

  }
  
  /**
   * @brief Esta funcion busca un producto en el array de productos, de acuerdo al termino eviado por parametro.
   * 
   * @param termino Termino o string que se utiliza para buscar elementos coincidentes sobre el array productos.
   */
  buscarProducto(termino:string) {
    let productosArr: JSON_Producto[] = [];
    
    this.termino = termino;
    this.productosSearch = this.testApi.db_buscarProducto(termino);
    this.api.getProductos(1, termino).subscribe(data => {
      productosArr = data;
      this.add_Productos(productosArr);
    }, error => {console.error(error.error);
    });
    
    /*this.T_api.APIMOCK_getProductos(1, termino).subscribe(data => {
      productosArr = data;
      //console.log(productosArr[0]);
      this.add_Productos(productosArr);
      console.log(this.productosSearch);

    }, error => {
      console.error(JSON.stringify(error));
    });*/

  }


  mapear_JSONProducto(producto:JSON_Producto):Producto {
        let new_product:Producto = {
          id_prod: producto.IdProd,
          nombre: producto.Nombre,
          codigo_barra: producto.CodBarra,
          img: producto.Imagen,
          tipo: producto.TipoProd,
          marca: producto.MarcaProd,
          precio_venta: producto.Precioventa,
          precio_costo: producto.PrecioCosto,
          porcentaje_utilidad: producto.PorcentajeGanancia,
        };
        return new_product;
  }


  add_Productos(arr:JSON_Producto[]) {
    for(let producto of arr) {
      let new_product:Producto = this.mapear_JSONProducto(producto);
      this.productosSearch.push( new_product );
    }
  }

}
