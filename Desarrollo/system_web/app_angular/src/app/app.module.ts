import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


/* Rutas */
import { APP_ROUTING } from './app.routes';

/* Servicios */
import { ProductosService } from './services/productos.service';

/* Componentes */
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/share/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { CardProductoComponent } from './components/cards/card-producto/card-producto.component';
import { NegociosComponent } from './pages/negocios/negocios.component';
import { SidebarAltComponent } from './components/share/sidebar-alt/sidebar-alt.component';
import { UserComponent } from './pages/user/user.component';
import { NavbarProdComponent } from './components/share/navbar-prod/navbar-prod.component';
import { ModalProdComponent } from './components/share/modals/modal-prod/modal-prod.component';
import { CardProductNewComponent } from './components/cards/card-product-new/card-product-new.component';
import { ModalProdNewComponent } from './components/share/modals/modal-prod-new/modal-prod-new.component';
import { BtnWarningModalComponent } from './components/share/modals/btn-warning-modal/btn-warning-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProductosComponent,
    CardProductoComponent,
    NegociosComponent,
    SidebarAltComponent,
    UserComponent,
    NavbarProdComponent,
    ModalProdComponent,
    CardProductNewComponent,
    ModalProdNewComponent,
    BtnWarningModalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [
    ProductosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
