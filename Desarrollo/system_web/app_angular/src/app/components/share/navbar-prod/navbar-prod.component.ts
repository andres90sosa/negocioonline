import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar-prod',
  templateUrl: './navbar-prod.component.html',
  styleUrls: ['./navbar-prod.component.css']
})
export class NavbarProdComponent implements OnInit {

  @Output() event_termino: EventEmitter<string>;

  constructor() { 
      this.event_termino = new EventEmitter();
    }

  ngOnInit(): void {
  }
  
  /**
   * @brief    Este metodo captura el termino de busqueda enviado por la barra de busqueda de l navbar-prod y lo emite al componente padre que lo requiera.
   * El envio se realiza a traves de la salida event_termino (Output) creada para dicho proposito.
   * 
   * @param p_termino   Termino o string de busqueda recibido desde la barra de busqueda de la navbar_pord.
   */
  navbarProd_buscar( p_termino:string) {
    this.event_termino.emit( p_termino );
  }
}
