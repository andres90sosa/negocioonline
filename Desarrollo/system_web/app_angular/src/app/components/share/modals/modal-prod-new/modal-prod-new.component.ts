import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal-prod-new',
  templateUrl: './modal-prod-new.component.html',
  styles: [
  ],
})
export class ModalProdNewComponent implements OnInit {

  /* Formulario de producto*/
  newProductForm = this.fb.group({
    nombre: ['', [Validators.required, Validators.minLength(3)]],
    codigo_barra: ['', [Validators.minLength(3)]],
    marca: ['', [Validators.minLength(3)]],
    precio_venta: ['', [Validators.min(0), Validators.max(10000000)]],
    precio_costo: ['', [Validators.min(0), Validators.max(10000000)]],
    porcentaje_utilidad: [''],
    tipos: this.fb.group({
      tipo_1: ['', [Validators.required, Validators.minLength(2)]],
      tipo_2: [''],
      tipo_3: [''],
      tipo_4: ['']
    }),
  });

  constructor( private fb:FormBuilder ) { }

  ngOnInit(): void {
  }

  guardar_producto() {
    console.log(this.newProductForm.get('nombre').value);
    console.log(this.newProductForm.get('codigo_barra').value);
    console.log(this.newProductForm.get('marca').value);
    console.log(this.newProductForm.get('precio_venta').value);
    console.log(this.newProductForm.get('precio_costo').value);
    console.log(this.newProductForm.get('porcentaje_utilidad').value);
    console.log(this.newProductForm.get('tipos').get('tipo_1').value);
  }

  limpiar_form() {
    this.newProductForm.get('nombre').setValue('');
    this.newProductForm.get('codigo_barra').setValue('');
    this.newProductForm.get('marca').setValue('');
    this.newProductForm.get('precio_venta').setValue('');
    this.newProductForm.get('precio_costo').setValue('');
    this.newProductForm.get('porcentaje_utilidad').setValue('');
    this.newProductForm.get('tipos').get('tipo_1').setValue('');
  }

  getf_porcentaje() {
    
    if((this.newProductForm.get('precio_venta').value >= 0)&&(this.newProductForm.get('precio_costo').value > 0)) {
      let porcentaje =  ((this.newProductForm.get('precio_venta').value / this.newProductForm.get('precio_costo').value) * 100) - 100;
      porcentaje = Math.round(porcentaje*10)/10;
      this.newProductForm.get('porcentaje_utilidad').setValue(porcentaje);
    }
  }

}
