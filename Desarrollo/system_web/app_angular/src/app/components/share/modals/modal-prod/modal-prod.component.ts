import { Component, OnInit, Input } from '@angular/core';
import { Producto } from '../../../../interfaces/producto.interface';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductosService } from '../../../../services/productos.service';
import { Const } from '../../../../../environments/environment';
import { ProductosTestsService } from '../../../../tests/services/productos-tests.service';

// jQuery Sign $
declare let $: any;


@Component({
  selector: 'app-modal-prod',
  templateUrl: './modal-prod.component.html'
})
export class ModalProdComponent implements OnInit {
  
  @Input() producto:Producto;
  @Input() id_producto:number;

  const_base64:string = Const.base64;
  default_img:string  = Const.default_img;
  
  /* Formulario de producto*/
  productoForm = this.fb.group({
    nombre: ['', [Validators.required, Validators.minLength(3)]],
    codigo_barra: ['', [Validators.minLength(3)]],
    marca: ['', [Validators.minLength(3)]],
    precio_venta: ['', [Validators.min(0), Validators.max(10000000)]],
    precio_costo: ['', [Validators.min(0), Validators.max(10000000)]],
    porcentaje_utilidad: [''],
    tipos: this.fb.group({
      tipo_1: ['', [Validators.required, Validators.minLength(2)]],
      tipo_2: [''],
      tipo_3: [''],
      tipo_4: ['']
    }),
  });
  
  constructor( private fb:FormBuilder, 
               private api: ProductosService,
               private testApi: ProductosTestsService) {
   }

  ngOnInit(): void {
    // this.autocompletar_form();
  }


  autocompletar_form() {
    this.productoForm.get('nombre').setValue(this.producto.nombre);
    this.productoForm.get('codigo_barra').setValue(this.producto.codigo_barra);
    this.productoForm.get('marca').setValue(this.producto.marca);
    this.productoForm.get('precio_venta').setValue(this.producto.precio_venta.toFixed(2));
    this.productoForm.get('precio_costo').setValue(this.producto.precio_costo.toFixed(2));
    this.productoForm.get('porcentaje_utilidad').setValue(this.get_porcentaje());
    this.productoForm.get('tipos').get('tipo_1').setValue(this.producto.tipo);
    
  }

  guardar_producto(id_prod:number) {

    let data_token:any;

    console.log(this.productoForm.get('nombre').value);
    console.log(this.productoForm.get('codigo_barra').value);
    console.log(this.productoForm.get('marca').value);
    console.log(this.productoForm.get('precio_venta').value);
    console.log(this.productoForm.get('precio_costo').value);
    console.log(this.productoForm.get('porcentaje_utilidad').value);
    console.log(this.productoForm.get('tipos').get('tipo_1').value);
    this.testApi.API_getToken().subscribe(data => {
      data_token = JSON.stringify(data);
      console.log(data_token);
    }, error => {
      console.log(JSON.stringify(error));
    });

    console.log(data_token);

    let idNameModal = '#productoModal' + id_prod;
    console.log(idNameModal);
    $(idNameModal).modal('toggle');
  }

  get_porcentaje() {
    let porcentaje = 0;

    if((this.productoForm.get('precio_venta').value >= 0)&&(this.productoForm.get('precio_costo').value > 0)) {
      porcentaje = ((this.productoForm.get('precio_venta').value / this.productoForm.get('precio_costo').value) * 100) - 100;
    }

    return porcentaje.toFixed(2);
  }

  getf_porcentaje() {
    this.productoForm.get('porcentaje_utilidad').setValue(this.get_porcentaje());
  }
  
}
