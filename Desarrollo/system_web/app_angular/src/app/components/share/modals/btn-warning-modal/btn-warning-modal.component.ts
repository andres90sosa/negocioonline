import { Component, OnInit, Input } from '@angular/core';

// jQuery Sign $
declare let $: any;

@Component({
  selector: 'app-btn-warning-modal',
  templateUrl: './btn-warning-modal.component.html',
  styles: [
  ],
})
export class BtnWarningModalComponent implements OnInit {

  @Input() i_idProducto:number;

  constructor() { }

  ngOnInit(): void {
  }

  config_modal() {
    // $('#ModalWarning_1').modal({backdrop: 'static', keyboard: false})
  }

  eliminar_producto(id_prod:number) {
    console.log("Eliminando producto: "+ id_prod);


    $('#ModalWarning_' + id_prod).modal('toggle');
    $('#productoModal' + id_prod).modal('toggle');
  }

}
