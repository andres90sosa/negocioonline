import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Producto } from '../../../interfaces/producto.interface';
import { Const } from '../../../../environments/environment';


@Component({
  selector: 'app-card-producto',
  templateUrl: './card-producto.component.html',
  styleUrls: ['./card-producto.component.css']
})
export class CardProductoComponent implements OnInit {

  @Input() producto:Producto;
  @Input() id_producto:number;

  const_base64:string = Const.base64;
  default_img:string  = Const.default_img;

  constructor( private fb:FormBuilder ) { }

  ngOnInit(): void {
  }

}
