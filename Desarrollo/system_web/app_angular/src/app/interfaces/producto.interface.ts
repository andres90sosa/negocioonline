import { Type, plainToClass } from "class-transformer";

export interface Producto{
    id_prod: number;
    nombre: string;
    codigo_barra: string;
    img: string;
    tipo: string;
    marca: string;
    precio_venta: number;
    precio_costo: number;
    porcentaje_utilidad: number;
}


export interface JSON_Producto{
    IdProd: number;
    Nombre: string;
    CodBarra: string;
    Imagen: string;
    TipoProd: string;
    MarcaProd: string;
    Precioventa: number;
    PrecioCosto: number;
    PorcentajeGanancia: number;
}