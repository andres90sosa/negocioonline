import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { UserComponent } from './pages/user/user.component';
import { NegociosComponent } from './pages/negocios/negocios.component';
import { ProductosComponent } from './pages/productos/productos.component';

const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'user', component: UserComponent },
    { path: 'negocios', component: NegociosComponent },
    { path: 'productos', component: ProductosComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
  ];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);