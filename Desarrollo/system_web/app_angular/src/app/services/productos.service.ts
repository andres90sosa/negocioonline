import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Producto, JSON_Producto } from '../interfaces/producto.interface';
import { environment } from '../../environments/environment';


@Injectable()
export class ProductosService {

    private productos: Producto[] = [];        
    private termino:string; /* termino de busqueda sobre array de productos.*/
    token:string;
    data_map;

    constructor( private http:HttpClient ) {
        console.log("Servicio productos, listo para utilizar.");
    }


    /**
     * @brief    Retorna el string que se escribe en el buscador de la navbar-prod.
     */
    getStrSearch():string {
        if(this.termino == null) {    /* Si busqueda es null, se busca por defecto.*/
            return "_start";
        }
        
        return this.termino;
    }


    getProductos(codNegocio: number, termino: string) {
        if ((termino == null || termino.trim() === '') && (codNegocio == null || codNegocio === 0)) {
            return null;
        } else {
            if (isNaN(Number(termino)))  {
                // buscamos por nombre de producto
                return this.http.get<JSON_Producto[]>(`${environment.url}/productos/obtenerproductos?CodNegocio=${codNegocio}&Nombre=${termino}&CodBarra=`);

            } else {
                // buscamos por código de barra del producto
                return this.http.get<JSON_Producto[]>(`${environment.url}/productos/obtenerproductos?CodNegocio=${codNegocio}&Nombre=&CodBarra=${termino}`);
            }
        }
    }
}

