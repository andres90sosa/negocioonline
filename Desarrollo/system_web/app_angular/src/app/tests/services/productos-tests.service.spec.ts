import { TestBed } from '@angular/core/testing';

import { ProductosTestsService } from './productos-tests.service';

describe('ProductosTestsService', () => {
  let service: ProductosTestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductosTestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
