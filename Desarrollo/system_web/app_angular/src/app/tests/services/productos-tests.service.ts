import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Producto, JSON_Producto } from '../../interfaces/producto.interface';


/* Constantes */
const url_api_mock = `https://be7d8eaa-fffd-4ca5-88c1-0f45370eb6fa.mock.pstmn.io/api/productos/obtenerproductos`;
const url_api      = `http://192.168.43.62:49809/api/productos/obtenerproductos`;

@Injectable({
  providedIn: 'root'
})
export class ProductosTestsService {

  private productos: Producto[] = [
            {
              id_prod: 101,
              nombre: 'Gaseosa Coca 2L',
              img: '',
              tipo: 'bebida',
              marca: 'Coca Company',
              precio_venta: 100,
              precio_costo: 70,
              porcentaje_utilidad: 40,
              codigo_barra: '10023006060907'
            },
            {
                id_prod: 102,
                nombre: 'Gaseosa Pepsi 2,5L',
                img: '',
                tipo: 'bebida',
                marca: 'Pepsico',
                precio_venta: 95,
                precio_costo: 70,
                porcentaje_utilidad: 35,
                codigo_barra: '10023006062002'
            },
            {
                id_prod: 103,
                nombre: 'Gaseosa Pritty Limon 2L',
                img: '',
                tipo: 'bebida',
                marca: 'Pretty',
                precio_venta: 85,
                precio_costo: 55,
                porcentaje_utilidad: 40,
                codigo_barra: '10023006061554'
            }
          ];
        
    private termino:string; /* termino de busqueda sobre array de productos.*/
    token:string;
    data_map;

    constructor( private http:HttpClient ) {
        console.log("Servicio listo para utilizar.");
    }

    API_getToken() {
      return this.http.get('https://be7d8eaa-fffd-4ca5-88c1-0f45370eb6fa.mock.pstmn.io/api/login/authenticate', {responseType: 'text'});
  }

  API_login() {
      
      /**Credenciales */
      
      /* URL de servicio de login en API NegocioOnline */
      // const url = '192.168.8.x:4989/api/login/authenticate';
      const url = 'https://be7d8eaa-fffd-4ca5-88c1-0f45370eb6fa.mock.pstmn.io/api/login/authenticate';

      /*Conexion al servicio de la API*/
      this.API_getToken().subscribe(data_token=> {
          this.token = data_token;
      }, error=>{
          console.log(JSON.stringify(error));
      });

      // Defino Headers que API de Negocio-Online necesita.
      // const headers = new HttpHeaders({
      // Authorization:
      //   'Bearer ${this.token}'
      // });
      
      console.log(this.token);
  }

  APIMOCK_getProductos(p_codNegocio:number, p_query:string) {
      const url = `${url_api}?pCodNegocio=${p_codNegocio}&pNombre=${p_query}&pCodBarra=`;
      console.log(url);

      return this.http.get<JSON_Producto[]>(url_api_mock);
  }
  
  /**
   * @brief    Retorna array con lista de producto cuyo nombres coinciden con el termino de busqueda enviado por parametro.
   * @param termino    Termino o string de busqueda utilizado para comparar con el nombre de los producto del array de productos obtenido de base de datos.
   */
  db_buscarProducto(termino:string):Producto[]{
      let productoArr:Producto[] = [];
      termino = termino.toLowerCase();
      this.termino = termino;

      if(termino === "") {
          return [];
      } else if(termino === "_start") {
          return [];
      }

      for(let producto of this.productos) {

          let nombre = producto.nombre.toLowerCase();
          
          if(nombre.indexOf(termino) >= 0) {
              productoArr.push( producto );
          }
      }
      return productoArr;
  }


  /**
   * @brief    Retorna el string que se escribe en el buscador de la navbar-prod.
   */
  getStrSearch():string {
      if(this.termino == null) {    /* Si busqueda es null, se busca por defecto.*/
          return "_start";
          //return "gaseosa";
      }
      
      return this.termino;
  }

  /**
   * @brief    Este metodo setea el termino de busqueda de acuerdo al string enviado por parametro.
   **/
  setStrSearch(termino:string) {
      this.termino = termino;
  }

  getProductos2(codNegocio: number, termino: string) {
      if ((termino == null || termino.trim() === '') && (codNegocio == null || codNegocio === 0)) {
          return null;
      } else {
          if (isNaN(Number(termino)))  {
              // buscamos por nombre de producto
              return this.http.get<JSON_Producto[]>(`${environment.url}/productos/obtenerproductos?CodNegocio=${codNegocio}&Nombre=${termino}&CodBarra=`);

          } else {
              // buscamos por código de barra del producto
              return this.http.get<JSON_Producto[]>(`${environment.url}/productos/obtenerproductos?CodNegocio=${codNegocio}&Nombre=&CodBarra=${termino}`);
          }
      }
  }
}
